<?php
require "template.php";

$Tanggal1 = date("Y-m-d");
$Tanggal2 = date("Y-m-d");

$Tanggal  = date("Y-m-d");

$dataakses = query("SELECT * FROM tabel_akses_2 WHERE TANGGAL = '$Tanggal' ORDER BY no DESC");
?>


<link href="fontawesome/css/all.css" rel="stylesheet">
<center>
  <h3>DATA AKSES RUANGAN</h3>
  <br>
  <div class="row mb-2">
    <div class="col">
      <div class="btn-group">
        <button type="button" class="btn btn-danger mx-2" href="#" data-toggle="modal" data-target="#filter" data-placement="bottom" title="Tanggal"><i class="fa fa-calendar"></i> Filter</button>
        <a href="dataruangan.php" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Data Ruangan"><i class="fa fa-door-open"></i> Data Ruangan</a>
      </div>
    </div>
    <div class="col">
      <!-- Export data -->
      <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" style="background:#008080; color:white"><i class="fa fa-download"></i> Export Data
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <a class="dropdown-item" href="pdfdataakses.php?Tanggal1=<?= $Tanggal1; ?>&Tanggal2=<?= $Tanggal2; ?>"><i class="fa fa-file-pdf"></i> Export to PDF</a>
          <a class="dropdown-item" href="exceldataakses.php?Tanggal1=<?= $Tanggal1; ?>&Tanggal2=<?= $Tanggal2; ?>"><i class="fa fa-file-excel"></i> Export to Excel</a>
        </div>
      </div>
    </div>
  </div>
  <div class="table-responsive-sm">
    <table class="table table-bordered table-hover table-dark table-striped">
      <tr class="text-center" style="background: purple;">
        <th>No.</th>
        <th>Tanggal</th>
        <th>ID Card</th>
        <th>Nama Pengguna</th>
        <th>Jam Masuk</th>
        <th>Jam Keluar</th>
        <th>Ruangan</th>
      </tr>
      <?php $i = 1; ?>

      <?php foreach ($dataakses as $akses) :
        $id_room     = $akses["id_room"];
        $dataroom    = query("SELECT * FROM tabel_room WHERE id_room = '$id_room'");
        $diff_tgl     = strtotime($akses["TANGGAL"]);
        $tanggal     = date("d F Y", $diff_tgl);
      ?>
        <tr>
          <td class="text-center"><?= $i; ?></td>
          <td class="text-center"><?= $tanggal; ?></td>
          <td class="text-center"><?= $akses["ID"]; ?></td>
          <td><?= $akses["NAMA"]; ?></td>
          <td class="text-center"><?= $akses["MASUK"]; ?></td>
          <?php

          if ($akses["KELUAR"] == 0) {
            echo '<td class="text-center"></td>';
          } else {
            echo '<td class="text-center">' . $akses["KELUAR"] . '</td>';
          }

          foreach ($dataroom as $room) {
            echo '<td>' . $room["room"] . '</td>';
          }
          ?>
        </tr>
        <?php $i++; ?>
      <?php endforeach; ?>

    </table>
  </div>
</center>


<!-- Modal Filter Tanggal -->
<div class="modal fade" id="filter" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger text-white">
        <h5 class="modal-title"><i class="fa fa-calendar"></i> FILTER TANGGAL</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="get" action="dataakses-filter.php">
        <div class="modal-body">
          <span>Tanggal Awal</span>
          <input type="date" class="form-control pickadate" name="TANGGAL1"><br><br>
          <span>Tanggal Akhir</span>
          <input type="date" class="form-control pickadate" name="TANGGAL2">
        </div>
        <div class="modal-footer">
          <button type="submit" value="Filter" class="btn btn-success"><i class="fa fa-filter"></i> Filter </button>
          <button type="reset" name="reset" class="btn text-white bg-warning"><i class="fa fa-sync-alt"></i>
            Reset</button>
          <button type="button" class=" btn btn-danger" data-dismiss="modal"> <i class="fa fa-undo"></i> Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>