<?php

require "template.php";

$data = query("SELECT * FROM tabel_anggota WHERE Level = 'Guru' ORDER BY NAMA ASC");
$date = date('Y-m-d');

$TOKEN   = $pengaturan["TOKEN"];

//cek apakah kolom idbaru pada tabel pengaturan kosong atau tidak
if ($pengaturan["idbaru"] !== "") {
  $sql = "UPDATE tabel_pengaturan SET idbaru = ''";
  $koneksi->query($sql);
}

if (isset($_POST["simpan"])) {
  if (tambahguru($_POST) > 0) {
    //kirim pesan telegram
    $ID_CHAT = $_POST["ID_CHAT"];
    $pesan = "SELAMAT BERGABUNG!!!\nData Diri Anda Berhasil ditambahkan\n\nID: " . $_POST['ID'] . "\nNama: " . $_POST["NAMA"] . "\nNo. Induk: " . $_POST["NO_INDUK"] . "\nGender: " . $_POST["KELAMIN"] . "\nPassword: " . $_POST["ID"] . "\n\nData ditambahkan pada: \n" . date("d F Y H:i:s") . "\n\nSegera laporkan ke admin jika terjadi kesalahan input data. Terimakasih";
    kirimPesan($ID_CHAT, $pesan, $TOKEN);
    //insert tabel_kehadiran
    // tambahkehadiran($_POST);
    echo "
                 <script> 
                  Swal.fire({ 
                  title: 'BERHASIL',
                  text: 'Data Telah disimpan',
                  icon: 'success', buttons: [false, 'OK'], 
                  }).then(function() { 
                      window.location.href='dataguru.php'; 
                  }); 
                 </script>
                ";
  } else {
    echo "
         <script> 
         Swal.fire({ 
            title: 'OOPS', 
            text: 'Data gagal ditambahkan', 
            icon: 'warning', 
            dangerMode: true, 
            buttons: [false, 'OK'], 
            }).then(function() { 
                window.location.href='dataguru.php'; 
            }); 
         </script>
        ";
  }
}




?>

<link href="fontawesome/css/all.css" rel="stylesheet">

<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row"></div>
    <div class="content-body">
      <section id="basic-datatable">
        <div class="row" id="table-hover-animation">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Data Guru</h4>
              </div>
              <div class="card-content">
                <div class="card-body card-dashboard">
                  <button type="button" class="btn btn-icon btn-info mr-1 mb-1" href="#tambahanggota" data-toggle="modal" data-placement="bottom" title="Registrasi" data-target="#tambahanggota"><i class="fa fa-user-plus"></i></button>
                  <a class="btn btn-icon btn-success mr-1 mb-1" href="broadcast_message.php"><i class="fa fa-envelope"></i></a>
                  <div class="dropleft float-right">
                    <button class="btn btn-secondary dropdown-toggle type=" button" data-toggle="dropdown" data-placement="bottom" title="Export">
                      <i class="fa fa-download"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="pdfanggota.php"><i class="fa fa-file-pdf"></i> PDF</a>
                      <a class="dropdown-item" href="excelanggota.php"><i class="fa fa-file-excel"></i> Excel</a>
                    </div>
                  </div>
                  <div id="tabelanggota">
                    <div class="table-responsive">
                      <table id="tabel-data" class="table table-bordered table-hover table-dark table-striped">
                        <thead>
                          <tr class="text-center" style="background: purple;">
                            <th>No</th>
                            <th>ID Card</th>
                            <th>ID Chat</th>
                            <th>NIP </th>
                            <th>Nama Guru</th>
                            <th>No Whatsapp</th>
                            <th>Terdaftar</th>
                            <th width="140px">Opsi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i = 1; ?>
                          <?php foreach ($data as $anggota) :
                            $diff_tgl  = strtotime($anggota["TERDAFTAR"]);
                            $terdaftar = date("d F Y", $diff_tgl);
                            $id_sub    = $anggota["id_sub"];
                            $datasub   = query("SELECT * FROM tabel_subject WHERE id_sub ='$id_sub' ");
                          ?>
                            <tr>
                              <td><?= $i; ?></td>
                              <td><?= $anggota["ID"]; ?></td>

                              <?php
                              if ($anggota["ID_CHAT"] == "") {
                                echo '<td class="text-center text-danger">--No ID Chat--</td>';
                              } else {
                                echo '<td class="text-center">' . $anggota["ID_CHAT"] . '</td>';
                              }
                              ?>

                              <td><?= $anggota["NO_INDUK"]; ?></td>
                              <td><?= $anggota["NAMA"]; ?></td>
                              <td><?= $anggota["WA_PRIBADI"]; ?></td>
                              <td><?= $terdaftar; ?></td>

                              <td align="center">
                                <div class="dropdown">
                                  <button class="btn btn-sm btn-primary dropdown-toggle" type="button" data-toggle="dropdown" data-placement="bottom" title="Opsi"><i class="fa fa-filter"></i></button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="ubahguru.php?ID=<?= $anggota["ID"]; ?>"><i class="fa fa-edit"></i> Edit</a>
                                    <a class="dropdown-item alert_hapus" href="hapus.php?ID=<?= $anggota["ID"]; ?>&ID_CHAT=<?= $anggota["ID_CHAT"]; ?>"><i class="fa fa-trash-alt"></i> Hapus</a>
                                    <a class="dropdown-item reset btn btn-sm" href="resetid.php?ID=<?= $anggota["ID"]; ?>"><i class="fa fa-id-card"></i> Reset ID</a>
                                  </div>
                                </div>
                              </td>

                            </tr>
                            <?php $i++; ?>
                          <?php endforeach; ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>


<!-- Modal Tambah Anggota -->
<div class="modal fade" id="tambahanggota" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-dark text-white">
        <h5 class="modal-title"><i class="fa fa-user-plus"></i> FORM TAMBAH GURU</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="dataguru.php" method="post">
        <div class="modal-body bg-dark text-white">
          <div class="form-group">
            <div class="idmasuk"></div><br>
            <input class="form-control bg-dark text-white" name="ID_CHAT" type="text" autocomplete="off" placeholder="ID Chat Bot Telegram"><br>
            <input class="form-control bg-dark text-white" name="NO_INDUK" type="text" autocomplete="off" placeholder="Nomor Induk Pegawai" required><br>
            <input class="form-control bg-dark text-white" name="NAMA" type="text" autocomplete="off" placeholder="Nama Lengkap" required><br>
            <input class="form-control bg-dark text-white" name="WA_PRIBADI" type="number" autocomplete="off" placeholder="Nomor Whatsapp Pribadi" required><br>

            <div class="row px-5">
              <div class="col">
                <div class="form-check">
                  <input class="form-check-input bg-dark text-white" type="radio" name="KELAMIN" value="L" required>
                  <label class="form-check-label">Laki laki</label>
                </div>
              </div>
              <div class="col">
                <div class="form-check">
                  <input class="form-check-input bg-dark text-white" type="radio" name="KELAMIN" value="P" required>
                  <label class="form-check-label">Perempuan</label>
                </div>
              </div>
            </div>
            <br>
          </div>
        </div>
        <div class="modal-footer bg-dark text-white">
          <button type="submit" name="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
          <button type="reset" name="reset" class="btn text-white btn-warning"><i class="fa fa-sync-alt"></i> Reset</button>
          <button type="button" class=" btn btn-danger" data-dismiss="modal"> <i class="fa fa-undo"></i> Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>


<!-- BEGIN: Page JS-->
<script src="template/app-assets/js/scripts/datatables/datatable.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
<!-- END: Page JS-->
<script src="js/script.js"></script>

<!-- script -->
<script>
  $(document).ready(function() {
    $('#tabel-data').DataTable();
  });
</script>
<script>
  var keyword = document.getElementById('keywordanggota');
  var tabelanggota = document.getElementById('tabelanggota');

  keyword.addEventListener('keyup', function() {
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {
      if (xhr.readyState == 4 && xhr.status == 200) {
        tabelanggota.innerHTML = xhr.responseText;
      }
    }

    xhr.open('GET', 'js/tabelanggota.php?keywordanggota=' + keyword.value, true);
    xhr.send();
  });
</script>