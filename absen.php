<?php

use chillerlan\QRCode\QRCode;

require './vendor/autoload.php';

$date  = date('Y-m-d');
$diff  = strtotime($date);
$tgl_f = date("d F Y", $diff);
?>

<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Selamat Datang</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <title>Sistem Absensi</title>
  <!-- <link rel="apple-touch-icon" href="template/app-assets/images/ico/apple-icon-120.png"> -->
  <link rel="shortcut icon" type="image/x-icon" href="img/SMAN1Cluring.png">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body class="m-0 p-0">
  <div class="container">
    <div class="jumbotron mb-0" style="height: 100vh;">
      <h1 class="display-4">Selamat Datang!</h1>
      <p class="lead">Silakan melakukan presensi dengan memindai QR Code dibawah ini</p>
      <hr class="my-4">
      <p class="text-center">Sesuaikan waktu Qr yang anda gunakan</p>
      <div class="row d-flex justify-content-center">
        <div class="col-md-5 d-flex justify-content-center">
          <div class="card" style="width: 18rem;">
            <div class="card-body text-center">
              <?= '<img src="' . (new QRCode)->render("datang-" . $date) . '" alt="QR Code" />'; ?>
              <h5 class="card-title">Absensi Kehadiran</h5>
            </div>
          </div>
        </div>
        <div class="col-md-5 d-flex justify-content-center">
          <div class="card" style="width: 18rem;">
            <div class="card-body text-center">
              <?= '<img src="' . (new QRCode)->render("pulang-" . $date) . '" alt="QR Code" />'; ?>
              <h5 class="card-title">Absensi Kepulangan</h5>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  </div>


</body>

</html>