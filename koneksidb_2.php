<?php

$server       = "localhost";
$user         = "root";
$password     = "";
$database     = "absen-cluring"; //Silakan ganti dengan nama database anda

$koneksi      = mysqli_connect($server, $user, $password, $database);


//Query tabel
$libur = query("SELECT * FROM tabel_hari_libur")[0];
$pengaturan = query("SELECT * FROM tabel_pengaturan")[0];

//Zona Waktu
$zona = "Asia/Jakarta";

date_default_timezone_set($zona);
if ($zona == 'Asia/Jakarta') {
  $det = 25200; //7 jam 
} else if ($zona == 'Asia/Makassar') {
  $det = 28800; //8 jam 
} else if ($zona == 'Asia/Jayapura') {
  $det = 32400; //9 jam 
}


function query($query)
{
  global $koneksi;
  $result = mysqli_query($koneksi, $query);
  $box = [];
  while ($siswa = mysqli_fetch_assoc($result)) {
    $box[] = $siswa;
  }
  return $box;
}


function tambahanggota($post)
{
  global $koneksi;
  $ID       = htmlspecialchars($post['ID']);
  $ID_CHAT  = htmlspecialchars($post['ID_CHAT']);
  $NO_INDUK = htmlspecialchars($post['NO_INDUK']);
  $NAMA     = htmlspecialchars($post['NAMA']);
  $KELAMIN  = htmlspecialchars($post['KELAMIN']);
  $id_sub   = htmlspecialchars($post['id_sub']);
  $TERDAFTAR = htmlspecialchars($post['TERDAFTAR']);
  //update gambar
  $gambar_anggota = upload();
  if (!$gambar_anggota) {
    return false;
  }
  //end update
  //insert data ke tabel_anggota
  $query = "INSERT INTO tabel_anggota
                   VALUES
                  ('$ID', '$ID_CHAT', '$NO_INDUK', '$NAMA', '$KELAMIN', '$id_sub', '$TERDAFTAR', '$gambar_anggota')
                  ";

  mysqli_query($koneksi, $query);
  return mysqli_affected_rows($koneksi);
}
//update gambar anggota
function upload()
{
  $file = $_FILES['gambar_anggota']['name'];
  $ukuran = $_FILES['gambar_anggota']['size'];
  $error = $_FILES['gambar_anggota']['error'];
  $tmpName = $_FILES['gambar_anggota']['tmp_name'];

  //cek apakah tidak ada gambar yang diupload
  if ($error === 4) {
    echo "<script>
                  alert('pilih gambar terlebih dahulu');
                </script>";
    return false;
  }
  //cek yang diupload adalah gambar
  $ekstensiGambarValid = ['jpg', 'jpeg', 'png'];
  $ekstensiGambar = explode('.', $file); //sahrul.jpg = ['sahrul', 'jpg']
  $ekstensiGambar = strtolower(end($ekstensiGambar));
  if (!in_array($ekstensiGambar, $ekstensiGambarValid)) {
    echo "<script>
                    alert('Yang Anda Upload Bukan Gambar !!');
                  </script>";
    return false;
  }
  //cek jika gambar ukurannya terlalu besar
  if ($ukuran > 2000000) {
    echo "<script>
                    alert('Ukuran Gambar terlalu Besar !!');
                  </script>";
    return false;
  }
  // gambar siap diupload
  //generate nama baru
  $namafilebaru = uniqid();
  $namafilebaru .= '.';
  $namafilebaru .= $ekstensiGambar;
  move_uploaded_file($tmpName, 'img/' . $namafilebaru);

  return $namafilebaru;
}
//end update gambar anggota
function tambahkehadiran($post)
{
  global $koneksi;
  $ID       = $post['ID'];
  $NO_INDUK = $post['NO_INDUK'];
  $NAMA     = $post['NAMA'];
  $TERDAFTAR = $post['TERDAFTAR'];

  //insert data ke tabel_kehadiran
  $query2 = "INSERT INTO tabel_kehadiran (ID, NO_INDUK, NAMA, TANGGAL, JAM_MASUK, CHECK_IN, LATE_IN, JAM_PULANG, CHECK_OUT, EARLY_OUT, KET)
                   VALUES
                  ('$ID', '$NO_INDUK', '$NAMA', '$TERDAFTAR', '', '', '', '', '', '', '')
                  ";

  mysqli_query($koneksi, $query2);
  return mysqli_affected_rows($koneksi);
}

function tambahsubject($post)
{
  global $koneksi;
  $SUBJECT  = htmlspecialchars($post['SUBJECT']);
  //insert data ke tabel_subject
  $query = "INSERT INTO tabel_subject(SUBJECT) VALUES('$SUBJECT')";
  mysqli_query($koneksi, $query);
  return mysqli_affected_rows($koneksi);
}

function ubahsubject($post)
{
  global $koneksi;
  $SUBJECT  = $post['SUBJECT'];
  $id_sub   = $post['id_sub'];

  //update data tabel_subject
  $query = "UPDATE tabel_subject SET SUBJECT = '$SUBJECT' WHERE id_sub = '$id_sub'";
  mysqli_query($koneksi, $query);
  return mysqli_affected_rows($koneksi);
}



function ubahanggota($post)
{
  global $koneksi;
  $ID          = $post['ID'];
  $ID_CHAT     = $post['ID_CHAT'];
  $NIS         = $post['NO_INDUK'];
  $NAMA        = $post['NAMA'];
  $KELAMIN     = $post['KELAMIN'];
  $id_sub      = $post['id_sub'];
  //update 8/2/2021
  $gambarLama  = $post['gambarLama'];


  if ($_FILES['gambar_anggota']['error'] === 4) {
    $gambar = $gambarLama;
  } else {
    $gambar_anggota = upload();
  }
  //end update 8/2/2021
  //update data ke tabel_siswa
  $query = "UPDATE tabel_anggota SET
              
              ID_CHAT   = '$ID_CHAT',
              NO_INDUK  = '$NIS',
              NAMA      = '$NAMA',
              KELAMIN   = '$KELAMIN',
              id_sub    = '$id_sub',
              gambar_anggota = '$gambar_anggota'

              WHERE ID  = '$ID'
                ";

  mysqli_query($koneksi, $query);
  return mysqli_affected_rows($koneksi);
}


function aturJam($post)
{
  global $koneksi;
  $ZONA           = $post['ZONA'];
  $JAM_MASUK_1    = $post['JAM_MASUK_1'];
  $JAM_MASUK_2    = $post['JAM_MASUK_2'];
  $JAM_MASUK_3    = $post['JAM_MASUK_3'];
  $JAM_PULANG_1   = $post['JAM_PULANG_1'];
  $JAM_PULANG_2   = $post['JAM_PULANG_2'];
  $JAM_PULANG_3   = $post['JAM_PULANG_3'];

  //insert data ke tabel_pengaturan
  $query = "UPDATE tabel_pengaturan SET 
                    ZONA         = '$ZONA',
                    JAM_MASUK_1  = '$JAM_MASUK_1',
                    JAM_MASUK_2  = '$JAM_MASUK_2',
                    JAM_MASUK_3  = '$JAM_MASUK_3',
                    JAM_PULANG_1 = '$JAM_PULANG_1',
                    JAM_PULANG_2 = '$JAM_PULANG_2',
                    JAM_PULANG_3 = '$JAM_PULANG_3'
                      ";

  mysqli_query($koneksi, $query);
  return mysqli_affected_rows($koneksi);
}

function aturToken($post)
{
  global $koneksi;
  $TOKEN      = $post['TOKEN'];
  $KEY_API    = $post['KEY_API'];

  //insert data ke tabel_pengaturan
  $query = "UPDATE tabel_pengaturan SET 
                       TOKEN   = '$TOKEN', KEY_API = '$KEY_API'";
  mysqli_query($koneksi, $query);
  return mysqli_affected_rows($koneksi);
}

function aturAdmin($post)
{
  global $koneksi;
  $pengaturan =  query("SELECT * FROM tabel_pengaturan")[0];
  $ID_CHAT    =  $post["ID_CHAT"];
  $username   =  $post['USERNAME'];
  $password   =  mysqli_real_escape_string($koneksi, $post["password"]);

  //cek password
  if (password_verify($password, $pengaturan["PASSWORD"])) {
    $query = "UPDATE tabel_pengaturan SET USERNAME = '$username', ID_CHAT = '$ID_CHAT'";
    mysqli_query($koneksi, $query);
    return mysqli_affected_rows($koneksi);
  }
}

function ubahPassword($post)
{
  global $koneksi;
  $pengaturan =  query("SELECT * FROM tabel_pengaturan")[0];
  $passlama   =  mysqli_real_escape_string($koneksi, $post["passlama"]);
  $passbaru   =  mysqli_real_escape_string($koneksi, $post["passbaru"]);
  $passbaru2  =  mysqli_real_escape_string($koneksi, $post["passbaru2"]);

  //cek password
  if (password_verify($passlama, $pengaturan["PASSWORD"]) and $passbaru == $passbaru2) {
    $password = password_hash($passbaru, PASSWORD_DEFAULT); //enkripsi password
    //set password baru ke tabel_pengaturan
    $query = "UPDATE tabel_pengaturan SET PASSWORD = '$password'";
    mysqli_query($koneksi, $query);
    return mysqli_affected_rows($koneksi);
  }
}

function aturLibur($post)
{
  global $koneksi;
  $HL_1   = $post['H_LIBUR_1'];
  $HL_2   = $post['H_LIBUR_2'];
  $TL_3   = $post['T_LIBUR_3'];
  $TL_4   = $post['T_LIBUR_4'];
  $TL_5   = $post['T_LIBUR_5'];
  $TL_6A  = $post['T_LIBUR_6A'];
  $TL_6B  = $post['T_LIBUR_6B'];
  $TL_7A  = $post['T_LIBUR_7A'];
  $TL_7B  = $post['T_LIBUR_7B'];


  //insert data ke tabel_hari_libur
  $query = "UPDATE tabel_hari_libur SET 
                    H_LIBUR_1   = '$HL_1',  H_LIBUR_2   = '$HL_2',
                    T_LIBUR_3   = '$TL_3',  T_LIBUR_4   = '$TL_4',
                    T_LIBUR_5   = '$TL_5',  T_LIBUR_6A  = '$TL_6A',  T_LIBUR_6B   = '$TL_6B',
                    T_LIBUR_7A  = '$TL_7A', T_LIBUR_7B = '$TL_7B' 
                      ";

  mysqli_query($koneksi, $query);
  return mysqli_affected_rows($koneksi);
}


function tapMasuk($jam_masuk, $TIME, $LATE_IN, $ket, $ID, $date)
{
  global $koneksi;
  $sql     = "UPDATE tabel_kehadiran SET JAM_MASUK = '$jam_masuk', CHECK_IN = '$TIME', LATE_IN = '$LATE_IN', KET = '$ket' WHERE ID = '$ID' AND TANGGAL = '$date'";
  $koneksi->query($sql);
  return;
}

function tapPulang($jam_pulang, $TIME, $EARLY_OUT, $ID, $date)
{
  global $koneksi;
  $sql     = "UPDATE tabel_kehadiran SET JAM_PULANG = '$jam_pulang', CHECK_OUT = '$TIME', EARLY_OUT = '$EARLY_OUT' WHERE ID = '$ID' AND TANGGAL = '$date'";
  $koneksi->query($sql);
  return;
}


function koreksikehadiran($post)
{
  global $koneksi;
  $no        = $post['no'];
  $KET       = $post['KET_2'];

  //update data ke tabel_zona
  $query = "UPDATE tabel_kehadiran SET KET = '$KET', STAT = 'locked' WHERE no = $no";
  mysqli_query($koneksi, $query);
  return mysqli_affected_rows($koneksi);
}



function hapusanggota($ID)
{
  global $koneksi;
  mysqli_query($koneksi, "DELETE FROM tabel_anggota WHERE ID = '$ID'");

  return mysqli_affected_rows($koneksi);
}

function hapussubject($id_sub)
{
  global $koneksi;
  mysqli_query($koneksi, "DELETE FROM tabel_subject WHERE id_sub = '$id_sub'");

  return mysqli_affected_rows($koneksi);
}


//function kirim pesan telegram
function kirimPesan($ID_CHAT, $pesan, $TOKEN)
{
  $url = "https://api.telegram.org/bot" . $TOKEN . "/sendMessage?parse_mode=markdown&chat_id=" . $ID_CHAT;
  $url = $url . "&text=" . urlencode($pesan);
  $ch = curl_init();
  $optArray = array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true
  );
  curl_setopt_array($ch, $optArray);
  $result = curl_exec($ch);
  curl_close($ch);
}
