<?php 
require "template.php";

$thn  = $_GET["tahun"];
$bln  = $_GET["bulan"];
$YM   = $thn."-".$bln;
$diff = strtotime($YM);
$TB   = date("F Y", $diff);  

$lengthday = cal_days_in_month(CAL_GREGORIAN, $bln, $thn); 

$dataanggota = query("SELECT * FROM tabel_anggota ORDER BY NAMA ASC"); 


?>



<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

      <style type="text/css">
          .hadir {
            color: green;
          }
          .sakit {
            color: orange;
          }
          .izin {
             color: blue;
          }
          .alfa{
            color: purple;
          }
          .bolos{
            color: brown;
          }
          .lupa{
            color: lightblue;
          }
          .libur {
            color: red;
          }
      </style>

  <center>
  	<h3>PRESENSI BULANAN</h3>
  	<br>

	<div class="row">
    <div class="col ml-5">
      <!-- Filter data -->
      <form method="get" action="">
        <div class="input-group">
          <div class="input-group-prepend"></div>
              <select name="bulan" class="custom-select col-md-4"><
                <option selected>---Pilih Bulan---</option>
                   <?php
                        $bulan = ["Januari" => "01", "Februari" => "02", "Maret" => "03", 
                                  "April" => "04", "Mei" => "05", "Juni" => "06",
                                  "Juli" => "07", "Agustus" => "08", "September" => "09",
                                  "Oktober" => "10", "November" => "11", "Desember" => "12"];
                        foreach ($bulan as $key => $val) {
                          echo "<option value=".$val.">".$key."</option>"; 
                        }
                     ?>
              </select>
              <input type="text" name="tahun" placeholder=" Masukkan Tahun..." autocomplete="off">
              <input type="submit" value="Filter">
          </div>
      </form>
    </div>

   <div class="col">
      <!-- Export data -->
        <div class="dropdown">
          <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"  style="background:#008080; color:white"><i class="fa fa-download"></i> Export Data
           </button>
           <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="pdfpresensibulanan.php?bulan=<?=$bln;?>&tahun=<?=$thn;?>"><i class="fa fa-file-pdf"></i> Export to PDF</a>
              <a class="dropdown-item" href="excelpresensibulanan.php?bulan=<?=$bln;?>&tahun=<?=$thn;?>"><i class="fa fa-file-excel"></i> Export to Excel</a>
           </div>
           <a href="rekapdata.php" class="btn btn-danger"><i class="fa fa-database"></i> Rekap Data</a>
      </div>
    </div>
  </div>

		<br>

    <div class="table-responsive-sm">

<div class="row" style="width:90rem;">
  <div class="col">
       <strong>Keterangan:
       <span class="mr-2 ml-2 hadir">Hadir;</span>
       <span class="mr-2 sakit">Sakit;</span>
       <span class="mr-2 izin">Izin;</span>
       <span class="mr-2 alfa">Alfa;</span>
       <span class="mr-2 bolos">Bolos;</span>
       <span class="mr-2 lupa">Lupa Tap;</span>
       <span class="mr-2 libur">Libur</span></strong>
  </div>
  <div class="col ml-3">
       <p style="font-weight: bold">Periode: <?=$TB; ?></p>
  </div>
</div>

<table class="table table-bordered table-hover table-striped" style="font-size: 11px;">
   <tr class="text-center text-white bg-dark"> 
   <th rowspan="2" class="py-3">No.</th>
   <th rowspan="2" class="py-3 px-5">Nama</th> 
   <th colspan="<?=$lengthday;?>" class="py-1">Tanggal</th>   
   </tr>
    <tr class="text-center text-white bg-dark">
       <?php  for($d=1; $d <= $lengthday; $d++){
         if ($d < 10){
                $d = "0".(String)$d;
              }
          echo "<th class='py-1'>$d</th>";
      }?>
   </tr>
<?php $i =1;?>

<tr> 
   <?php 

      foreach ($dataanggota as $anggota) :
        $ID   = $anggota["ID"];
        $nama = $anggota["NAMA"];
        echo "<td>".$i."</td>
              <td>".$nama."</td>"; 

           for ($d=1; $d<=$lengthday; $d++) { 
             if ($d < 10){
                $d = "0".(String)$d;
              }
             
             $tgl  = date("Y-m-".$d, $diff);
             $read = query("SELECT * FROM tabel_kehadiran WHERE ID = '$ID' AND TANGGAL = '$tgl'");

              if($read){
                foreach ($read as $key) {
                    switch ($key['KET']) {
                       case 'HADIR': $col = "green";     break;
                       case 'SAKIT': $col = "yellow";    break;
                       case 'IZIN' : $col = "blue";      break;
                       case 'ALFA' : $col = "purple";    break;
                       case 'BOLOS': $col = "brown";     break;
                       case 'LUPA' : $col = "lightblue"; break;
                       case 'LIBUR': $col = "red";       break;
                       case ''     : $col = "";          break;
                    }
                       echo '<td style="background-color:'.$col.';"></td>';
                 }
              }
              else{
                echo '<td>-</td>';
              }

           }
    ?>
</tr>
   <?php $i++; ?>
  <?php endforeach;?>
</table>
</div>

  	


  </center>

  
	

</body>
</html>