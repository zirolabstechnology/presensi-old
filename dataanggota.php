<?php

require "template.php";

$data = query("SELECT * FROM tabel_anggota WHERE Level = 'Anggota' ORDER BY NAMA ASC");
$date = date('Y-m-d');

$TOKEN   = $pengaturan["TOKEN"];

//cek apakah kolom idbaru pada tabel pengaturan kosong atau tidak
if ($pengaturan["idbaru"] !== "") {
  $sql = "UPDATE tabel_pengaturan SET idbaru = ''";
  $koneksi->query($sql);
}

if (isset($_POST["simpan"])) {
  if (tambahanggota($_POST) > 0) {
    //kirim pesan telegram
    $id_sub  = $_POST["id_sub"];
    $datasub = query("SELECT * FROM tabel_subject WHERE id_sub = '$id_sub'")[0];
    $ID_CHAT = $_POST["ID_CHAT"];
    $pesan = "SELAMAT BERGABUNG!!!\nData Diri Anda Berhasil ditambahkan\n\n Nama: " . $_POST["NAMA"] . "\n No. Induk: " . $_POST["NO_INDUK"] . "\n Gender: " . $_POST["KELAMIN"] . "\n Subject: " . $datasub["SUBJECT"] . "\n\nData ditambahkan pada: \n" . date("d F Y H:i:s") . "\n\nSegera laporkan ke admin jika terjadi kesalahan input data. Terimakasih";
    kirimpesan($ID_CHAT, $pesan, $TOKEN);
    //insert tabel_kehadiran
    tambahkehadiran($_POST);
    echo "
                 <script> 
                  Swal.fire({ 
                  title: 'BERHASIL',
                  text: 'Data Telah disimpan',
                  icon: 'success', buttons: [false, 'OK'], 
                  }).then(function() { 
                      window.location.href='dataanggota.php'; 
                  }); 
                 </script>
                ";
  } else {
    echo "
         <script> 
         Swal.fire({ 
            title: 'OOPS', 
            text: 'Data gagal ditambahkan', 
            icon: 'warning', 
            dangerMode: true, 
            buttons: [false, 'OK'], 
            }).then(function() { 
                window.location.href='dataanggota.php'; 
            }); 
         </script>
        ";
  }
}




?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title></title>


</head>

<body>

  <!--  -->
  <center>
    <h3>DATA ANGGOTA</h3>
    <br>

    <div class="row">
      <div class="col">
        <!-- Tombol tambah data -->
        <div class="btn-group">
          <button type="button" class="tambah btn" href="#tambahanggota" style="background:#2E8B57; color:white;" data-toggle="modal" data-target="#tambahanggota"><i class="fa fa-plus"></i> Tambah Data</button>
          <a href="subject.php" class="btn mx-4" style="background:darkblue; color:white;"><i class="fa fa-users"></i> Data Kelas</a>
        </div>
      </div>
      <div class="col">
        <!-- Search Data -->
        <div class="form-group">
          <input class="form-control" id="keywordanggota" placeholder="Masukkan Keyword Pencarian Data di sini..." autocomplete="off" type="text">
        </div>
        <!--  -->
      </div>
      <div class="col">
        <!-- Export data -->
        <div class="dropdown">
          <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" style="background:#008080; color:white"><i class="fa fa-download"></i> Export Data
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="pdfanggota.php"><i class="fa fa-file-pdf"></i> Export to PDF</a>
            <a class="dropdown-item" href="excelanggota.php"><i class="fa fa-file-excel"></i> Export to Excel</a>
          </div>
        </div>
      </div>
    </div>

    <div id="tabelanggota">
      <div class="table-responsive-sm">
        <table class="table table-bordered table-hover table-dark table-striped">
          <tr class="text-center" style="background: purple;">
            <th>No.</th>
            <th>ID Card</th>
            <th>ID Chat</th>
            <th>No. Induk</th>
            <th>Nama Anggota</th>
            <th width="10px">L/P</th>
            <th>WA Pribadi</th>
            <th>WA Ortu</th>
            <th>Kelas</th>
            <th>Terdaftar</th>
            <th width="140px">Opsi</th>
          </tr>
          <?php $i = 1; ?>

          <?php foreach ($data as $anggota) :
            $diff_tgl  = strtotime($anggota["TERDAFTAR"]);
            $terdaftar = date("d F Y", $diff_tgl);
            $id_sub = $anggota["id_sub"];
            $datasub = query("SELECT * FROM tabel_subject WHERE id_sub ='$id_sub' ");
          ?>
            <tr>
              <td class="text-center"><?= $i; ?></td>
              <td class="text-center"><?= $anggota["ID"]; ?></td>
              <?php
              if ($anggota["ID_CHAT"] == "") {
                echo '<td class="text-center text-danger">--No ID Chat--</td>';
              } else {
                echo '<td class="text-center">' . $anggota["ID_CHAT"] . '</td>';
              }
              ?>
              <td class="text-center"><?= $anggota["NO_INDUK"]; ?></td>
              <td><?= $anggota["NAMA"]; ?></td>
              <td class="text-center"><?= $anggota["KELAMIN"]; ?></td>
              <td> <?= $anggota['WA_PRIBADI'] ?></td>
              <td> <?= $anggota['WA_ORTU'] ?></td>
              <?php
              if ($anggota["id_sub"] == 0) {
                echo '<td class="text-center text-danger">--No Subject--</td>';
              } else {
                foreach ($datasub as $subject) {
                  echo '<td class="text-center">' . $subject["SUBJECT"] . '</td>';
                }
              }
              ?>
              <td class="text-center"><?= $terdaftar; ?></td>
              <td align="center">
                <!-- <a class="kehadiran btn btn-primary btn-sm" href="kehadiranperorang.php?ID=<?= $anggota["ID"]; ?>&NAMA=<?= $anggota["NAMA"]; ?>&ID_CHAT=<?= $anggota["ID_CHAT"]; ?>"><i class="fa fa-clipboard"></i></a>
                <a class="ubah btn btn-success btn-sm" href="ubahanggota.php?ID=<?= $anggota["ID"]; ?>"><i class="fa fa-edit"></i></a>
                <a class="hapus btn btn-danger btn-sm alert_hapus" href="hapus.php?ID=<?= $anggota["ID"]; ?>&ID_CHAT=<?= $anggota["ID_CHAT"]; ?>"><i class="fa fa-trash-alt"></i></a>
              </td> -->
                <div class="dropdown">
                  <button class="btn btn-sm btn-primary dropdown-toggle" type="button" data-toggle="dropdown" data-placement="bottom" title="Opsi"><i class="fa fa-filter"></i></button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="kehadiranperorang.php?ID=<?= $anggota["ID"]; ?>&NAMA=<?= $anggota["NAMA"]; ?>&ID_CHAT=<?= $anggota["ID_CHAT"]; ?>"><i class="fa fa-clipboard"></i>
                      Kehadiran</a>
                    <a class="dropdown-item" href="aksesperorang.php?Template=template.php&ID=<?= $anggota["ID"]; ?>&NAMA=<?= $anggota["NAMA"]; ?>"><i class="fa fa-door-open"></i>
                      Akses</a>
                    <a class="dropdown-item" href="permit.php?Template=template.php&ID=<?= $anggota["ID"]; ?>"><i class="fa fa-envelope"></i>
                      Permit</a>
                    <a class="dropdown-item" href="ubahanggota.php?ID=<?= $anggota["ID"]; ?>"><i class="fa fa-edit"></i> Edit</a>
                    <a class="dropdown-item alert_hapus" href="hapus.php?ID=<?= $anggota["ID"]; ?>&ID_CHAT=<?= $anggota["ID_CHAT"]; ?>"><i class="fa fa-trash-alt"></i> Hapus</a>
                    <a class="dropdown-item reset btn btn-sm" href="resetid.php?ID=<?= $anggota["ID"]; ?>"><i class="fa fa-id-card"></i> Reset ID</a>
                  </div>
                </div>
              </td>
            </tr>
            <?php $i++; ?>
          <?php endforeach; ?>

        </table>
      </div>
    </div>

  </center>

  <!-- Modal Tambah Anggota -->
  <div class="modal fade" id="tambahanggota" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-dark text-white">
          <h5 class="modal-title">FORM TAMBAH ANGGOTA</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="dataanggota.php" method="post" enctype="multipart/form-data">
          <div class="modal-body bg-dark text-white">
            <div class="form-group">
              <div class="idmasuk"></div><br>
              <input class="form-control bg-dark text-white" name="ID_CHAT" type="text" autocomplete="off" placeholder="ID Chat Bot Telegram"><br>
              <input class="form-control bg-dark text-white" name="NO_INDUK" type="text" autocomplete="off" placeholder="Nomor Induk Pegawai" required><br>
              <input class="form-control bg-dark text-white" name="NAMA" type="text" autocomplete="off" placeholder="Nama Lengkap" required><br>
              <input class="form-control bg-dark text-white" name="WA_PRIBADI" type="number" autocomplete="off" placeholder="Nomor Whatsapp Pribadi" required><br>
              <input class="form-control bg-dark text-white" name="WA_ORTU" type="number" autocomplete="off" placeholder="Nomor Whatsapp Orang Tua" required><br>
              <div class="row px-5 mb-3">
                <div class="col">
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="KELAMIN" value="L" required>
                    <label class="form-check-label">Laki laki</label>
                  </div>
                </div>
                <div class="col">
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="KELAMIN" value="P" required>
                    <label class="form-check-label">Perempuan</label>
                  </div>
                </div>
              </div>
              <div class="input-group form-group">
                <div class="input-group-prepend mr-2"></div>
                <select name="id_sub" class="custom-select col-md-5 bg-dark text-white">
                  < <option selected>---Pilih Kelas---</option>

                    <?php
                    $subject = query("SELECT * FROM tabel_subject");
                    foreach ($subject as $i) {
                      echo "<option value=" . $i['id_sub'] . ">" . $i['SUBJECT'] . "</option>";
                    } ?>

                </select>
                <div class="input-group-prepend ml-2"></div>
                <select name="id_shift" class="custom-select col-md-5 bg-dark text-white">
                  < <option selected>---Pilih Shift---</option>

                    <?php
                    $shift = query("SELECT * FROM tabel_shift");
                    foreach ($shift as $i) {
                      $masuk  = $i["JAM_MASUK_2"];
                      $pulang = $i["JAM_PULANG_2"];
                      echo "<option value=" . $i['id_shift'] . ">" . $i['id_shift'] .
                        " (" . $masuk . "-" . $pulang . ")</option>";
                    } ?>

                </select>
              </div>
              <input type="text" name="TERDAFTAR" value="<?= $date; ?>" hidden>
            </div>
          </div>
          <div class="modal-footer bg-dark text-white">
            <button type="submit" name="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
            <button type="reset" name="reset" class="btn text-white" style="background: blue"><i class="fa fa-sync-alt"></i> Reset</button>
            <button type="button" class=" btn btn-danger" data-dismiss="modal"> <i class="fa fa-undo"></i> Batal</button>
          </div>
        </form>
      </div>
    </div>
  </div>

</body>

</html>