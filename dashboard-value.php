<?php

require "koneksidb.php";

//Waktu
$date  = date('Y-m-d');
$diff  = strtotime($date);
$tgl_f = date("d F Y", $diff);
$clock = date('H:i:s');

//Total Anggota
$query      = "SELECT * FROM tabel_anggota";
$result     = mysqli_query($koneksi, $query);
$anggota    = mysqli_num_rows($result);

//Total Hadir
$query2      = "SELECT * FROM tabel_kehadiran WHERE TANGGAL = '$date' AND KET = 'HADIR'";
$result2     = mysqli_query($koneksi, $query2);
$hadir       = mysqli_num_rows($result2);

//Total Tidak Hadir
$query3      = "SELECT * FROM tabel_kehadiran WHERE TANGGAL = '$date' AND 
                (KET = 'ALFA' OR KET = 'SAKIT' OR KET ='IZIN' OR KET = 'BOLOS')";
$result3     = mysqli_query($koneksi, $query3);
$absen       = mysqli_num_rows($result3);

//Total Terlambat
$query4      = "SELECT * FROM tabel_kehadiran WHERE TANGGAL = '$date' AND LATE_IN != 0
                AND CHECK_IN != '00:00:00'";
$result4     = mysqli_query($koneksi, $query4);
$terlambat   = mysqli_num_rows($result4);

//Total check out
$query5      = "SELECT * FROM tabel_kehadiran WHERE TANGGAL = '$date' AND CHECK_OUT != ''
                AND CHECK_OUT != 'empty'";
$result5     = mysqli_query($koneksi, $query5);
$check_out   = mysqli_num_rows($result5);

//Total check in
$check_in    = $hadir - $check_out;

//Suhu Tubuh Tertinggi Hari ini
$query6   = "SELECT MAX(suhu) AS suhu_max FROM tabel_kehadiran WHERE TANGGAL = '$date'";
$result6  = mysqli_query($koneksi, $query6);
while ($row = mysqli_fetch_assoc($result6)) {
  $suhu = $row["suhu_max"];
}

//Total ruangan
$query7     = "SELECT * FROM tabel_room";
$result7    = mysqli_query($koneksi, $query7);
$room       = mysqli_num_rows($result7);





?>

<!DOCTYPE html>
<html>

<head>
  <title></title>
</head>

<body>
  <center>



    <div class="container">
      <div class=" table-responsive-sm">
        <table class="table table-bordered">
          <tr style="font-size:18px;" align="center">
            <td><i class="fa fa-calendar"></i> <?= $tgl_f; ?></td>
            <td><i class="fa fa-clock"></i> <?= $clock; ?></td>
          </tr>
        </table>
      </div>

      <div class="row my-4 ">

        <div class="col-sm-4">
          <div class="card text-white  mb-3" style="background-color: darkblue">
            <div class="card-header" style="font-size:20px;">Total Anggota</div>
            <div class="card-body" style="font-size: 72px;"><i class="fa fa-users"></i> <?= $anggota; ?> </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="card text-white bg-success mb-3">
            <div class="card-header" style="font-size: 20px;">Total Hadir</div>
            <div class="card-body" style="font-size: 72px;"><i class="fa fa-user-check"></i> <?= $hadir; ?></div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="card text-white bg-danger mb-3">
            <div class="card-header" style="font-size:20px;">Total Tidak Hadir</div>
            <div class="card-body" style="font-size: 72px;"><i class="fa fa-user-times"></i> <?= $absen; ?></div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="card text-white mb-3" style="background-color: purple">
            <div class="card-header" style="font-size: 20px;">Total Terlambat</div>
            <div class="card-body" style="font-size: 75px;"><i class="fa fa-user-clock"></i> <?= $terlambat; ?></div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="card text-white bg-primary mb-3">
            <div class="card-header" style="font-size:20px;">Total Check In</div>
            <div class="card-body" style="font-size: 75px;"><i class="fa fa-sign-in-alt"></i> <?= $check_in; ?></div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="card text-white mb-3" style="background-color: orange">
            <div class="card-header" style="font-size: 20px;">Total Check Out</div>
            <div class="card-body" style="font-size: 75px;"><i class="fa fa-sign-out-alt"></i> <?= $check_out; ?></div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="card text-white bg-secondary mb-3">
            <div class="card-header" style="font-size: 20px;">Total Ruangan</div>
            <div class="card-body" style="font-size: 75px;"><i class="fa fa-building"></i> <?= $room; ?></div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="card text-white mb-3" style="background-color: red">
            <div class="card-header" style="font-size: 20px;">Suhu Tertinggi</div>
            <div class="card-body" style="font-size: 75px;"><i class="fa fa-thermometer-half"></i> <?= $suhu; ?></div>
          </div>
        </div>



      </div>
    </div>



    </div>
    </div>
    </div>

  </center>


</body>

</html>