<!-- <!DOCTYPE html>
<html lang="en">

<head></head>

<body>
  <script src="template/app-assets/vendors/js/vendors.min.js"></script> -->
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<!-- <script src="template/app-assets/vendors/js/charts/apexcharts.min.js"></script>
  <script src="template/app-assets/vendors/js/extensions/tether.min.js"></script>
  <script src="template/app-assets/vendors/js/extensions/shepherd.min.js"></script> -->
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<!-- <script src="template/app-assets/js/core/app-menu.js"></script>
  <script src="template/app-assets/js/core/app.js"></script>
  <script src="template/app-assets/js/scripts/components.js"></script> -->
<!-- END: Theme JS-->
<!-- <script src="js/script.js"></script> -->
<!-- sweetalert -->
<!-- <script src="template/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
  <script src="template/app-assets/vendors/js/extensions/polyfill.min.js"></script>

  <script src="template/app-assets/js/scripts/extensions/sweet-alerts.js"></script>
</body> -->

<!-- </html> -->
<?php

use Mpdf\Tag\Time;

// require 'koneksidb.php';
require 'send_wa.php';
require "template_2.php";

// session_start();

// if (!isset($_SESSION["Level"]) == "Anggota") {
//   header("Location:index.php");
//   exit;
// }


if ($_POST['data'] != null) {
  $TANGGAL1  = date("Y-m-d");
  $TANGGAL2  = date("Y-m-d");

  $ID       = $_SESSION["ID"];
  $data     = query("SELECT * FROM tabel_anggota WHERE ID = '$ID'")[0];
  $id_shift = $data['id_shift'];
  $datashift = query("SELECT * FROM tabel_shift WHERE id_shift = '$id_shift'")[0];
  $NAMA     = $data["NAMA"];
  $ID_CHAT  = $data["ID_CHAT"];

  $libur      = query("SELECT * FROM tabel_hari_libur")[0];
  date_default_timezone_set("Asia/Jakarta");
  $now        = date('H:i:s'); //Jam Saat ini
  $date       = date("Y-m-d"); //Tanggal hari ini
  $today      = date("l"); //Nama hari ini


  $clock = time();
  // example time interval
  // $interval = $time1->diff($time2);
  // echo $interval->format('%s second(s)');

  $datakehadiran = query("SELECT * FROM tabel_kehadiran WHERE ID = '$ID' AND TANGGAL BETWEEN '$TANGGAL1' AND '$TANGGAL2' ORDER BY no DESC");
}
?>

<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row"></div>
    <div class="content-body">
      <div class="container">
        <?php
        if ($_POST['data'] == 'datang-' . $date) {
          // cek kondisi terlambat atau tidak
          if ($clock < strtotime($datashift['JAM_MASUK_1'])) {
            $late_in = 0;
            $stats = "Masuk";
          } else {
            $late_in = $clock - strtotime($datashift['JAM_MASUK_1']);
            $stats = "Terlambat";
          }

          // update field kehadiran
          absenDatang($id_shift, $now, $late_in, 'HADIR', $ID, $date, 36, $stats);
          // mengirim pesan whatsapp kepada orang tua
          send($data['WA_ORTU'], "Anak anda bernama " . $data['NAMA'] . ", telah datang ke sekolah dan melakukan presensi");
          echo "
    <script>
    Swal.fire({ 
            title: 'SELAMAT',
            text: 'Absen Datang Berhasil',
            icon: 'success', 
            }).then(function() { 
            window.location.href='kehadiran_2.php'; 
            }); 
  </script>
    ";
        } elseif ($_POST['data'] == 'pulang-' . $date) {
          if ($clock >= strtotime($datashift['JAM_PULANG_1']) && $datakehadiran[0]['STAT'] == "Masuk") {
            $early_out = 0;
            $stats = "Masuk";
          } elseif ($clock >= strtotime($datashift['JAM_PULANG_1']) && $datakehadiran[0]['STAT'] == "Terlambat") {
            $early_out = 0;
            $stats = "Terlambat";
          } else {
            $early_out = strtotime($datashift['JAM_PULANG_1']) - time();
            $stats = "Pulang Awal";
          }
          // update query sql tb kehadiran untuk absen pulang
          absenPulang($now, $early_out, $ID, $date, 35, $stats);
          echo "
    <script>
    Swal.fire({ 
            title: 'SELAMAT',
            text: 'Absen Pulang Berhasil',
            icon: 'success', buttons: [false, 'OK'], 
            }).then(function() { 
            window.location.href='kehadiran_2.php'; 
            }); 
  </script>
    ";

          send($data['WA_ORTU'], "Anak anda bernama " . $data['NAMA'] . ", Telah melakukan absen pulang pada pukul " . $now);
        } else {
          echo "<p> Scan QR Absen </p>";
        }
        ?>
      </div>
    </div>
  </div>