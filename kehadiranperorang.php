<?php 

require "template.php";

if(isset($_GET["TANGGAL1"]) AND isset($_GET["TANGGAL2"])){
  $TANGGAL1  = $_GET["TANGGAL1"];
  $TANGGAL2  = $_GET["TANGGAL2"];
}
else{
  $TANGGAL1  = date("Y-m-d");
  $TANGGAL2  = date("Y-m-d");
}

$ID        = $_GET["ID"];
$NAMA      = $_GET["NAMA"];
$ID_CHAT   = $_GET["ID_CHAT"];

$data = query("SELECT * FROM tabel_kehadiran WHERE ID = '$ID' AND TANGGAL BETWEEN '$TANGGAL1' AND '$TANGGAL2' ORDER BY no DESC");



 ?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title></title>
 </head>
 <body>
   <center>
 	<h3>REKAMAN PRESENSI</h3>
 	<br>
 	<div class="col">
 		<table class="table">
 			<tr>
 				<th>ID Card :</th>
 				<td><?=$ID;?></td>
 				<th>Nama :</th>
 				<td><?=$NAMA;?></td>
 				<td><form method="get" action="kehadiranperorang.php">
				         <input type="date" name="TANGGAL1"> s/d
				         <input type="date" name="TANGGAL2">
				         <input type="text" name="ID" value="<?=$ID;?>" hidden>
				         <input type="text" name="NAMA" value="<?=$NAMA;?>" hidden>
				          <input type="text" name="ID_CHAT" value="<?=$ID_CHAT;?>" hidden>
				         <input type="submit" value="Filter">
				    </form>
				</td>
				<td>
					<div class="dropdown">
				        <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"  style="background:#008080; color:white"><i class="fa fa-download"></i> Export Data
				        </button>
				        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
				          <a class="dropdown-item" href="pdfperorang.php?ID=<?=$ID;?>&NAMA=<?=$NAMA;?>&TANGGAL1=<?=$TANGGAL1;?>&TANGGAL2=<?=$TANGGAL2;?>"><i class="fa fa-file-pdf"></i> Export to PDF</a>
				          <a class="dropdown-item" href="excelperorang.php?ID=<?=$ID;?>&NAMA=<?=$NAMA;?>&TANGGAL1=<?=$TANGGAL1;?>&TANGGAL2=<?=$TANGGAL2;?>"><i class="fa fa-file-excel"></i> Export to Excel</a>
				        </div>
				     </div>
				</td>
 			</tr>
 		</table>
 	</div>
 	<div class="table-responsive-sm mx-5 my-3">
 		<table class="table table-bordered table-striped text-center">
 			<tr class="bg-dark text-white">
 				<th class="py-3" rowspan="2">Tanggal</th>
 				<th class="py-1" colspan="3">Jam Masuk</th>
 			    <th class="py-1" colspan="3">Jam Pulang</th>
 			    <th class="py-3" rowspan="2">Keterangan</th>
 			    <th class="py-3" rowspan="2">Aksi</th>
 			</tr>
 			<tr class="bg-dark text-white">
 				<th class="py-1">Masuk</th>
 				<th class="py-1">Check In</th>
 				<th class="py-1">Late In</th>
 				<th class="py-1">Pulang</th>
 				<th class="py-1">Check Out</th>
 				<th class="py-1">Early Out</th>
 			</tr>
 	<?php foreach ($data as $kehadiran) :
 		 $diff_tgl = strtotime($kehadiran["TANGGAL"]);
         $tanggal  = date("d F Y", $diff_tgl);
 		 $f_late_in   = date("H:i:s", $kehadiran["LATE_IN"] - $det);
         $f_early_out = date("H:i:s", $kehadiran["EARLY_OUT"] - $det);
 	?>
 			<tr>
 				<td><?=$tanggal;?></td>
 				<td><?=$kehadiran["JAM_MASUK"];?></td>
 				<td><?=$kehadiran["CHECK_IN"];?></td>
 				<td><?=$f_late_in;?></td>
 				<td><?=$kehadiran["JAM_PULANG"];?></td>
 				<td><?=$kehadiran["CHECK_OUT"];?></td>
 				<td><?=$f_early_out;?></td>
 				<td><?=$kehadiran["KET"];?></td>
 				<td>
 				<a class="ubah btn btn-success btn-sm"  href="koreksikehadiran.php?no=<?=$kehadiran["no"];?>&
 					ID_CHAT=<?=$ID_CHAT;?>"><i class="fa fa-edit"></i> Koreksi</a>
 				</td>
 			</tr>
 	<?php endforeach; ?>
 		</table>
 	</div>
 </center>
 </body>
 </html>