<?php
require "template.php";

if (isset($_GET["bulan"]) and isset($_GET["tahun"])) {
  $bln = $_GET["bulan"];
  $thn = $_GET["tahun"];
} else {
  $bln = date("m"); //bulan saat ini
  $thn = date("Y"); //tahun saat ini
}

$tgl_1 = $thn . "-" . $bln . "-01";
$tgl_2 = $thn . "-" . $bln . "-31";

switch ($bln) {
  case '01':
    $month = "Januari";
    break;
  case '07':
    $month = "Juli";
    break;
  case '02':
    $month = "Februari";
    break;
  case '08':
    $month = "Agustus";
    break;
  case '03':
    $month = "Maret";
    break;
  case '09':
    $month = "September";
    break;
  case '04':
    $month = "April";
    break;
  case '10':
    $month = "Oktober";
    break;
  case '05':
    $month = "Mei";
    break;
  case '11':
    $month = "November";
    break;
  case '06':
    $month = "Juni";
    break;
  case '12':
    $month = "Desember";
    break;
}

$dataanggota = query("SELECT * FROM tabel_anggota ORDER BY NAMA ASC");

?>



<!DOCTYPE html>
<html>

<head>
  <title></title>
</head>

<body>
  <center>
    <h3>REKAPITULASI PRESENSI BULANAN</h3>
    <br>

    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <!-- Filter data -->
          <form method="get" action="rekapdata.php">
            <div class="input-group">
              <div class="input-group-prepend"></div>
              <select name="bulan" class="custom-select">
                < <option selected>---Pilih Bulan---</option>
                  <?php
                  $bulan = [
                    "Januari" => "01", "Februari" => "02", "Maret" => "03",
                    "April" => "04", "Mei" => "05", "Juni" => "06",
                    "Juli" => "07", "Agustus" => "08", "September" => "09",
                    "Oktober" => "10", "November" => "11", "Desember" => "12"
                  ];

                  foreach ($bulan as $key => $val) {
                    echo "<option value=" . $val . ">" . $key . "</option>";
                  }
                  ?>
              </select>
              <input type="text" name="tahun" placeholder=" Masukkan Tahun..." autocomplete="off">
              <input type="submit" value="Filter">
            </div>
          </form>
        </div>

        <div class="col-md-4">
          <!-- Export data -->
          <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" style="background:#008080; color:white"><i class="fa fa-download"></i> Export Data
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="pdfrekap.php?bulan=<?= $bln; ?>&tahun=<?= $thn; ?>"><i class="fa fa-file-pdf"></i> Export to PDF</a>
              <a class="dropdown-item" href="excelrekap.php?bulan=<?= $bln; ?>&tahun=<?= $thn; ?>"><i class="fa fa-file-excel"></i> Export to Excel</a>
            </div>
            <a href="presensibulanan.php?tahun=<?= $thn; ?>&bulan=<?= $bln; ?>" class="btn btn-warning"><i class="fa fa-edit"></i> Detail</a>
          </div>
        </div>
      </div>
    </div>

    <br>

    <div class="table-responsive-sm mx-5">
      <div class="row">
        <div class="col-md-10">
          <p class="text-left" style="font-style: italic;">H = Hadir; S = Sakit; I = Izin; A = Alfa; B = Bolos; L = Lupa tap; T = Terlambat; P = Pulang cepat;</p>
        </div>
        <div class="col-md-2">
          <p style="font-weight: bold">Periode: <?= $month . " " . $thn; ?></p>
        </div>
      </div>

      <table class="table table-bordered table-hover table-striped">
        <tr class="text-center text-white bg-dark">
          <th>No.</th>
          <th>No. Induk</th>
          <th>Nama Anggota</th>
          <th>H</th>
          <th>S</th>
          <th>I</th>
          <th>A</th>
          <th>B</th>
          <th>L</th>
          <th>T</th>
          <th>P</th>
          <th>Late In</th>
          <th>Early Out</th>
        </tr>
        <?php $i = 1; ?>

        <?php

        foreach ($dataanggota as $anggota) :
          $ID = $anggota["ID"];

          //Menghitung jumlah ALFA
          $query =  "SELECT * FROM tabel_kehadiran WHERE ID = '$ID' AND KET = 'ALFA' AND TANGGAL BETWEEN '$tgl_1' AND '$tgl_2'";
          $result   = mysqli_query($koneksi, $query);
          $alfa     = mysqli_num_rows($result);

          //menghitung jumlah SAKIT
          $query2 = "SELECT * FROM tabel_kehadiran WHERE ID = '$ID' AND KET = 'SAKIT' AND TANGGAL BETWEEN '$tgl_1' AND '$tgl_2'";
          $result2   = mysqli_query($koneksi, $query2);
          $sakit     = mysqli_num_rows($result2);

          //menghitung jumlah IZIN
          $query3 = "SELECT * FROM tabel_kehadiran WHERE ID = '$ID' AND KET = 'IZIN' AND TANGGAL BETWEEN '$tgl_1' AND '$tgl_2'";
          $result3   = mysqli_query($koneksi, $query3);
          $izin      = mysqli_num_rows($result3);

          //menghitung jumlah BOLOS
          $query4 = "SELECT * FROM tabel_kehadiran WHERE ID = '$ID' AND KET = 'BOLOS' AND TANGGAL BETWEEN '$tgl_1' AND '$tgl_2'";
          $result4   = mysqli_query($koneksi, $query4);
          $bolos     = mysqli_num_rows($result4);

          //menghitung jumlah LUPA
          $query5 = "SELECT * FROM tabel_kehadiran WHERE ID = '$ID' AND KET = 'LUPA' AND TANGGAL BETWEEN '$tgl_1' AND '$tgl_2'";
          $result5   = mysqli_query($koneksi, $query5);
          $lupa     = mysqli_num_rows($result5);

          //Menghitung jumlah HADIR
          $query6 = "SELECT * FROM tabel_kehadiran WHERE ID = '$ID' AND KET = 'HADIR' AND TANGGAL BETWEEN '$tgl_1' AND '$tgl_2'";
          $result6   = mysqli_query($koneksi, $query6);
          $hadir     = mysqli_num_rows($result6);

          //Menghitung jumlah TERLAMBAT
          $query7      = "SELECT * FROM tabel_kehadiran WHERE ID = '$ID' AND LATE_IN > 0 AND TANGGAL BETWEEN '$tgl_1' AND '$tgl_2'";
          $result7     = mysqli_query($koneksi, $query7);
          $terlambat   = mysqli_num_rows($result7);

          //Menghitung jumlah PULANG CEPAT
          $query8      = "SELECT * FROM tabel_kehadiran WHERE ID = '$ID' AND EARLY_OUT > 0 AND TANGGAL BETWEEN '$tgl_1' AND '$tgl_2'";
          $result8     = mysqli_query($koneksi, $query8);
          $plg_cepat   = mysqli_num_rows($result8);

          //Menghitung jumlah JAM TERLAMBAT
          $query9      = "SELECT SUM(LATE_IN) AS 'sum_late' FROM tabel_kehadiran WHERE ID = '$ID' AND LATE_IN != 0 AND CHECK_IN != '00:00:00' AND TANGGAL BETWEEN '$tgl_1' AND '$tgl_2' ";
          $result9     = mysqli_query($koneksi, $query9);
          $late_in     = mysqli_fetch_array($result9);
          $f_late_in   = date("H:i:s", $late_in["sum_late"] - $det);

          //Menghitung jumlah JAM PULANG CEPAT
          $query10      = "SELECT SUM(EARLY_OUT) AS 'sum_early' FROM tabel_kehadiran WHERE ID = '$ID' AND EARLY_OUT != 0 AND CHECK_OUT != '00:00:00' AND TANGGAL BETWEEN '$tgl_1' AND '$tgl_2'";
          $result10     = mysqli_query($koneksi, $query10);
          $early_out    = mysqli_fetch_array($result10);
          $f_early_out   = date("H:i:s", $early_out["sum_early"] - $det);
        ?>


          <tr>
            <td class="text-center"><?= $i; ?></td>
            <td class="text-center"><?= $anggota["NO_INDUK"]; ?></td>
            <td><?= $anggota["NAMA"]; ?></td>
            <td class="text-center"><?= $hadir ?></td>
            <td class="text-center"><?= $sakit; ?></td>
            <td class="text-center"><?= $izin; ?></td>
            <td class="text-center"><?= $alfa; ?></td>
            <td class="text-center"><?= $bolos; ?></td>
            <td class="text-center"><?= $lupa; ?></td>
            <td class="text-center"><?= $terlambat; ?></td>
            <td class="text-center"><?= $plg_cepat; ?></td>
            <td class="text-center"><?= $f_late_in; ?></td>
            <td class="text-center"><?= $f_early_out; ?></td>
          </tr>
          <?php $i++; ?>
        <?php endforeach; ?>
      </table>
    </div>




  </center>




</body>

</html>