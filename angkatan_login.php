
<?php 
	include("koneksidb.php");
require_once 'angkaToRomawi.php';
  $date  = date('Y-m-d');
  $diff  = strtotime($date); $tgl_f = date("d F Y", $diff);
  $clock = date('H:i:s');
  
  
$subject = query("SELECT * FROM tabel_subject");
 ?>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
        content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords"
        content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Cek Absen</title>
    <link rel="shortcut icon" type="image/x-icon" href="img/SMAN1Cluring.png">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

     <link rel="stylesheet" type="text/css" href="template/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/vendors/css/charts/apexcharts.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/vendors/css/extensions/tether-theme-arrows.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/vendors/css/extensions/tether.min.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/vendors/css/extensions/shepherd-theme-default.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/components.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/pages/dashboard-analytics.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/pages/card-analytics.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/plugins/tour/tour.css">
    <!-- END: Page CSS-->
    <!-- datatables-->
    <link rel="stylesheet" type="text/css" href="template/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/bootstrap-extended.css">
    
    <!-- BEGIN: Page CSS-->
    <!-- <link rel="stylesheet" type="text/css" href="template/app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/pages/dashboard-analytics.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/pages/card-analytics.css">
    <link rel="stylesheet" type="text/css" href="template/app-assets/css/plugins/tour/tour.css"> -->
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="template/assets/css/style.css">
    <!-- END: Custom CSS-->

</head>

<body>

    <br>
    <div class="container">
        <div class=" table-responsive-sm">
            <table class="table table-bordered">
                <tr style="font-size:18px;" align="center">
                    <td><i class="fa fa-calendar"></i> <?=$tgl_f;?>
                    || <i class="feather icon-clock"></i> <strong id="clock"><?php echo date("H:i:s");?></strong>
                    </td>
                </tr>
            </table>
        </div>
    </div>


    <div class="container">
        <div class="card">
            <div class="card-header">
              <h4 class="card-title">Sistem Absensi <strong class="text-primary">SMAN 1 Cluring</strong></h4>
              <img class="round" src="img/SMAN1Cluring.png" alt="avatar" height="40" width="40">
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="tabel-data" class="table zero-configuration">
                    <!--    <thead>-->
                            <?php
                    $subject = query("SELECT DISTINCT ANGKATAN FROM tabel_subject WHERE ANGKATAN IS NOT NULL");
                    foreach ($subject as $i) {
                         echo '
                                <tr>
                               
                                <td class="text-center">
                                <a class="text-primary font-weight-bold" href="angkatan_2.php?ANGKATAN='.$i["ANGKATAN"].'">
                                <span class="menu-item" data-i18n="Angkatan">Angkatan '.convertRomawi($i['ANGKATAN']).'</span></a>
                                </td>
                               
                                </tr>
                            ';
                                } ?> 
                                <tr>
                                    <td class="text-center">
                                        
                                    <a href="cekabsen.php" class="text-primary font-weight-bold">
                                        <span class="menu-item">Cek Semua Absensi </span></a>
                                    </td>
                                </tr>
                    <!--    </thead>-->
                    </table>
                </div>
                <br>
                <a  href="index.php" class="btn btn-primary btn-inline">
                <i class="feather icon-log-in"></i>
                Kembali ke Login
                </a>
            </div>
        </div>
    </div>
    
                        <script>
                            $(document).ready(function () {
                                $('#tabel-data').DataTable();
                            });
                        </script>
                            <!-- BEGIN: Vendor JS-->
    <script src="template/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="template/app-assets/vendors/js/charts/apexcharts.min.js"></script>
    <script src="template/app-assets/vendors/js/extensions/tether.min.js"></script>
    <script src="template/app-assets/vendors/js/extensions/shepherd.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="template/app-assets/js/core/app-menu.js"></script>
    <script src="template/app-assets/js/core/app.js"></script>
    <script src="template/app-assets/js/scripts/components.js"></script>
    <!-- END: Theme JS-->
<!-- BEGIN: Page JS-->
    <script src="template/app-assets/js/scripts/datatables/datatable.js"></script>
    <script src="template/app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="template/app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <script src="template/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="template/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="template/app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="template/app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="template/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
    <script src="template/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
    <!-- END: Page JS-->
<script>
// Function ini dijalankan ketika Halaman ini dibuka pada browser
$(function(){
setInterval(clock, 1000);//fungsi yang dijalan setiap detik, 1000 = 1 detik
});
 


var serverClock = jQuery("#clock");
if (serverClock.length > 0) {
    showServerTime(serverClock, serverClock.text());
}
 
function showServerTime(obj, time) {
    var parts   = time.split(":"),
        newTime = new Date();

    newTime.setHours(parseInt(parts[0], 10));
    newTime.setMinutes(parseInt(parts[1], 10));
    newTime.setSeconds(parseInt(parts[2], 10));

    var timeDifference  = new Date().getTime() - newTime.getTime();

    var methods = { 
        displayTime: function () {
            var now = new Date(new Date().getTime() - timeDifference);
            obj.text([
                methods.leadZeros(now.getHours(), 2),
                methods.leadZeros(now.getMinutes(), 2),
                methods.leadZeros(now.getSeconds(), 2)
            ].join(":"));
            setTimeout(methods.displayTime, 500);
        },
 
        leadZeros: function (time, width) {
            while (String(time).length < width) {
                time = "0" + time;
            }
            return time;
        }
    }
    methods.displayTime();
}

</script>
</body>

</html>