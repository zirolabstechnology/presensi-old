<?php
require 'template.php';
?>

<script src="qr-scanner/js/jquery.min.js"></script>
<script src="qr-scanner/js/bootstrap.min.js"></script>

<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row"></div>
    <div class="content-body text-center">
      <div class="col-md-12 col-md-offset-4">
        <div class="panel panel-danger">
          <div class="panel-heading">
            <h3 class="panel-title">Selamat Datang!</h3>
          </div>
          <div class="panel-body text-center">
            <canvas></canvas>
            <hr>
            <select></select>
          </div>
          <div class="panel-footer">
            <center><a class="btn btn-danger mt-1" href="kehadiran_2.php">Kembali</a></center>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Js Lib -->

<script type="text/javascript" src="qr-scanner/js/jquery.js"></script>
<script type="text/javascript" src="qr-scanner/js/qrcodelib.js"></script>
<script type="text/javascript" src="qr-scanner/js/webcodecamjquery.js"></script>
<script type="text/javascript">
  var arg = {
    resultFunction: function(result) {
      //$('.hasilscan').append($('<input name="noijazah" value=' + result.code + ' readonly><input type="submit" value="Cek"/>'));
      // $.post("../cek.php", { noijazah: result.code} );
      var redirect = 'qr_absen.php';
      $.redirectPost(redirect, {
        data: result.code
      });
    }
  };

  var decoder = $("canvas").WebCodeCamJQuery(arg).data().plugin_WebCodeCamJQuery;
  decoder.buildSelectMenu("select");
  decoder.play();
  /*  Without visible select menu
      decoder.buildSelectMenu(document.createElement('select'), 'environment|back').init(arg).play();
  */
  $('select').on('change', function() {
    decoder.stop().play();
  });

  // jquery extend function
  $.extend({
    redirectPost: function(location, args) {
      var form = '';
      $.each(args, function(key, value) {
        form += '<input type="hidden" name="' + key + '" value="' + value + '">';
      });
      $('<form action="' + location + '" method="POST">' + form + '</form>').appendTo('body').submit();
    }
  });
</script>