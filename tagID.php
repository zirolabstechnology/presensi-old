<?php 	
	require "template.php";

  $date = date('Y-m-d');

       if(isset($_GET["reject"])){
         echo "
           <script> 
             Swal.fire({ 
                title: 'Tap ditolak!!!', 
                text: 'Silakan cek data ID anda!!!', 
                icon: 'warning', 
                dangerMode: true, 
                buttons: [false, 'OK'], 
                }).then(function() { 
                    window.location.href='tagID.php'; 
                }); 
            </script>
              ";
    
      }

       if(isset($_GET["masuk"])){
         $NAMA = $_GET["NAMA"]; 
         echo "
           <script> 
             Swal.fire({ 
                title: 'Tap Masuk Diterima', 
                text: '$NAMA', 
                icon: 'success', 
                buttons: [false, 'OK'], 
                }).then(function() { 
                    window.location.href='kehadiran.php'; 
                }); 
            </script>
              ";
      }

      if(isset($_GET["pulang"])){
        $NAMA = $_GET["NAMA"];
         echo "
           <script> 
             Swal.fire({ 
                title: 'Tap Pulang Diterima', 
                text: '$NAMA', 
                icon: 'success', 
                buttons: [false, 'OK'], 
                }).then(function() { 
                   window.location.href='kehadiran.php'; 
                }); 
            </script>
              ";
      }

 ?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title>	</title>
 </head>
 <body>
 	<center>
    <img class="img-fluid responsive-sm" src="img/rfid.jpg" alt="Responsive image" style="width:300px; height:180px;">
 		<h4>SILAKAN TAP KARTU ANDA</h4>

 	   <div class="container my-5" style="width:20rem;">
 		<form method="get" action="prosesID.php">	
		 		<div class="form-group">
					<div class="input-group mb-3">
					  <div class="input-group-prepend"><span class="input-group-text">ID Card</span></div>
					  <input type="text" autocomplete="off" class="form-control"name = "ID" placeholder="Masukkan ID Anda" required>
					</div>
  		 		  <div class="input-group mb-3">
  					  <input type="text" autocomplete="off" class="form-control"name = "KEY_API" placeholder="Masukkan Key API" value="<?=$pengaturan["KEY_API"]?>" hidden>
  					</div>
		     </div>
				<button type="submit" name="kirim" class="btn btn-success"><i class="fa fa-edit"></i> Submit</button>
        <button type="reset" name="reset" class="btn btn-danger"><i class="fa fa-undo"></i> Reset</button> 
          </form>
          <br>
         <?php  
               if(isset($_GET["unregister"])){
                 $ID = $_GET["ID"];
                 echo ' 
                    <div class="alert alert-danger alert-dismissible fade show  role="alert">  
                        <h5>ID Tidak Terdaftar!!!</h5>
                            <button type="button" class="tambah btn btn-primary" href="#tambahanggota" data-toggle="modal"data-target="#tambahanggota">Registrasi ID</button>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div> 
                   ';
              }
          ?>
		</div>     

 	</center>

 	
      <!-- Modal Tambah Anggota -->
<div class="modal fade" id="tambahanggota" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-dark text-white">
        <h5 class="modal-title">FORM TAMBAH ANGGOTA</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="dataanggota.php" method="post">
         <div class="modal-body bg-dark text-white">
                    <div class="form-group">
                      <div class="idmasuk"></div><br>
                          <input class="form-control bg-dark text-white" name="ID_CHAT" type="text" autocomplete="off" placeholder="ID Chat Bot Telegram"><br>
                          <input class="form-control bg-dark text-white" name="NO_INDUK" type="text" autocomplete="off" placeholder="Nomor Induk Pegawai" required><br>
                          <input class="form-control bg-dark text-white" name="NAMA" type="text" autocomplete="off" placeholder="Nama Lengkap" required><br>

                          <div class="row px-5">
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="radio" name="KELAMIN" value="L" required>
                                <label class="form-check-label">Laki laki</label>
                              </div>
                            </div>
                            <div class="col">
                               <div class="form-check">
                                  <input class="form-check-input" type="radio" name="KELAMIN" value="P" required>
                                  <label class="form-check-label">Perempuan</label>
                              </div>
                            </div>
                          </div>
                            
                    <br>
                    <div class="input-group">
                        <div class="input-group-prepend"></div>
                           <select name="id_sub" class="custom-select col-md-5 bg-dark text-white"><
                            <option selected>---Pilih Subject---</option>

                            <?php
                             $subject = query("SELECT * FROM tabel_subject");
                             foreach ($subject as $i) {
                                echo "<option value=".$i['id_sub'].">".$i['SUBJECT']."</option>"; 
                            } ?> 

                          </select>
                    </div>
                        <input type="text" name="TERDAFTAR" value="<?=$date;?>" hidden>
            </div>  
      </div>
      <div class="modal-footer bg-dark text-white">
        <button type="submit" name="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
        <button type="reset" name="reset" class="btn text-white" style="background: blue"><i class="fa fa-sync-alt"></i> Reset</button>
        <button type="button" class=" btn btn-danger" data-dismiss="modal"> <i class="fa fa-undo"></i> Batal</button>
      </div>
     </form>
    </div>
  </div>
</div>







 </body>
 </html>