<?php 	
	require "template.php";

	$ID_CHAT = $pengaturan["ID_CHAT"];
	$TOKEN   = $pengaturan["TOKEN"];

	//Cek tombol submit apa sudah ditekan atau belum
if(isset($_POST["simpan"]))  { //pengaturan jam kerja
    if(aturJam($_POST) > 0) {
    	if($_POST["ZONA"]=="Asia/Jakarta"){
    		$zona = "WIB";
    	}
    	if($_POST["ZONA"]=="Asia/Makassar"){
    		$zona = "WITA";
    	}
    	if($_POST["ZONA"]=="Asia/Jayapura"){
    		$zona = "WIT";
    	}
    	$pesan = "Data Jam Kerja Berhasil diperbarui\n\nZona Waktu  : ".$zona.
    	          "\nMulai Masuk : ".$_POST["JAM_MASUK_1"]."\nJam Masuk   : ".$_POST["JAM_MASUK_2"].
    	          "\nAkhir Masuk  : ".$_POST["JAM_MASUK_3"]."\nMulai Pulang : ".$_POST["JAM_PULANG_1"].
    	          "\nJam Pulang   : ".$_POST["JAM_PULANG_2"]."\nAkhir Pulang  : ".$_POST["JAM_PULANG_3"];
            echo "
                 <script>
				  Swal.fire({ 
                  title: 'SELAMAT',
                  text: 'Data jam kerja telah berhasil disimpan',
                  icon: 'success', buttons: [false, 'OK'], 
                  }).then(function() { 
                  window.location.href='aturjam.php'; 
                  }); 
			     </script>
                ";      
    }
    else {
    	$pesan = "Data Jam Kerja gagal Disimpan!!!";
		      echo "
		        <script> 
		         Swal.fire({ 
		            title: 'OOPS', 
		            text: 'Data jam kerja telah gagal disimpan!!!', 
		            icon: 'warning', 
		            dangerMode: true, 
		            buttons: [false, 'OK'], 
		            }).then(function() { 
		                window.location.href='aturjam.php'; 
		            }); 
		         </script>
		        ";
    }
    if($pengaturan["SW_2"] == 1){
         kirimpesan($ID_CHAT, $pesan, $TOKEN);
      }
 } 

 ?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title>	</title>
 </head>
 <body>
 	<center>
 		<h3>PANEL JAM KERJA</h3>

 	   <div class="table-responsive-sm my-4">
 		<form method="post" action="aturjam.php">	
 			<table class="table table-bordered" style="width:25rem;">
              	            <tr>
					    		<td>Zona Waktu</td>
					    		<td><div class="form-group-sm">
			                        <div class="input-group-sm">
			                           <select name="ZONA" class="custom-select">
			                           	<?php 
			                           		if($pengaturan["ZONA"] == "Asia/Jakarta"){
			                           			$select1 = "selected";
			                           			$select2 = "";
			                           			$select3 = "";
			                           		}
			                           		if($pengaturan["ZONA"] == "Asia/Makassar"){
			                           			$select1 = "";
			                           			$select2 = "selected";
			                           			$select3 = "";
			                           		}
			                           		if($pengaturan["ZONA"] == "Asia/Jayapura"){
			                           			$select1 = "";
			                           			$select2 = "";
			                           			$select3 = "selected";
			                           		}
			                           	 ?>
			                             <option>---Pilih Zona Waktu---</option>
			                      		 <option <?=$select1?> value="Asia/Jakarta">WIB</option>
			                             <option <?=$select2?> value="Asia/Makassar">WITA</option>
			                             <option <?=$select3?> value="Asia/Jayapura">WIT</option>
			                            </select>
			                        </div>
			                    </td>
					    	</tr>
					    	<tr>
					    		<td>Mulai Masuk</td>
					    		<td><input class="form-control" name="JAM_MASUK_1" type="text" autocomplete="off" value="<?=$pengaturan["JAM_MASUK_1"]?>"></td>
					    	</tr>
					    	<tr>
					    		<td>Jam Masuk</td>
					    		<td><input class="form-control" name="JAM_MASUK_2" type="text" autocomplete="off" value="<?=$pengaturan["JAM_MASUK_2"]?>"></td>
					    	</tr>
					    	<tr>
					    		<td>Akhir Masuk</td>
					    		<td><input class="form-control" name="JAM_MASUK_3" type="text" autocomplete="off" value="<?=$pengaturan["JAM_MASUK_3"]?>"></td>
					    	</tr>
					    	<tr>
					    		<td>Mulai Pulang</td>
					    		<td><input class="form-control" name="JAM_PULANG_1" type="text" autocomplete="off" value="<?=$pengaturan["JAM_PULANG_1"]?>"></td>
					    	</tr>
					    	<tr>
					    		<td>Jam Pulang</td>
					    		<td><input class="form-control" name="JAM_PULANG_2" type="text" autocomplete="off" value="<?=$pengaturan["JAM_PULANG_2"]?>"></td>
					    	</tr>
					    	<tr>
					    		<td>Akhir Pulang</td>
					    		<td><input class="form-control" name="JAM_PULANG_3" type="text" autocomplete="off" value="<?=$pengaturan["JAM_PULANG_3"]?>"></td>
					    	</tr>
					    </table> 
					     <button type="submit" name="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                         <button type="submit" name="reset" class="btn btn-danger"><i class="fa fa-undo"></i> Reset</button>
                      </form>
		</div>     

 	</center>
 </body>
 </html>