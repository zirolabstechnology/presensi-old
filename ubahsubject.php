<?php
require "template.php";

if (isset($_POST["ubahsubject"])) {
	if (ubahsubject($_POST) > 0) {
		echo "
                 <script> 
			        Swal.fire({ 
			            title: 'BERHASIL',
			            text: 'Nama Subject berhasil diubah',
			            icon: 'success', buttons: [false, 'OK'], 
			            }).then(function() { 
			                window.location.href='subject.php'; 
			            });  
				</script>
                ";
	} else {
		echo "
         <script> 
         Swal.fire({ 
            title: 'OOPS', 
            text: 'Nama Subject Gagal diubah!!!', 
            icon: 'warning', 
            dangerMode: true, 
            buttons: [false, 'OK'], 
            }).then(function() { 
                window.location.href='subject.php'; 
            }); 
         </script>
        ";
	}
}



?>

<!DOCTYPE html>
<html>

<head>
	<title>Pengaturan</title>
</head>

<body>
	<center>
		<h3>UBAH DATA KELAS </h3>
		<br>

		<?php
		if (isset($_GET["id_sub"])) {
			$id_sub = mysqli_escape_string($koneksi, $_GET["id_sub"]);
			$data   = query("SELECT * FROM tabel_subject WHERE id_sub = '$id_sub'")[0];
			//Menghitung jumlah subject
			$sub      = $data["SUBJECT"];
			$query    = "SELECT * FROM tabel_anggota WHERE id_sub = '$id_sub'";
			$result   = mysqli_query($koneksi, $query);
			$val      = mysqli_num_rows($result);
		?>

			<div class="container responsive-sm" style="width: 50rem;">
				<form method="post" action="ubahsubject.php">
					<table class="table table-striped text-center">
						<tr class="text-white bg-dark">
							<th>Subject</th>
							<th>Angkatan</th>
							<th>Wali Kelas</th>
							<th>Jumlah</th>
						</tr>
						<tr>
							<td><input class="form-control bg-dark text-white" type="text" name="SUBJECT" value="<?= $sub; ?>" autocomplete="off"></td>
							<td>
								<input class="form-control bg-dark text-white" name="ANGKATAN" type="number" min="1" autocomplete="off" placeholder="Masukkan Angkatan" value="<?= $data['ANGKATAN'] ?>" required>
							</td>
							<td>
								<select name="GURU_ID" class="custom-select bg-dark text-white">
									<?php
									$subject = query("SELECT * FROM tabel_anggota WHERE Level = 'Guru'");
									foreach ($subject as $i) {
										if ($data['GURU_ID'] == $i['ID']) {
											echo "<option selected value=" . $i['ID'] . ">" . $i['NAMA'] . "</option>";
										} else {
											echo "<option value=" . $i['ID'] . ">" . $i['NAMA'] . "</option>";
										}
									}
									?>

								</select>
							</td>
							<td> <input type="text" class="form-control bg-dark text-white" disabled value="<?= $val; ?> Orang"></td>
							<input type="text" name="id_sub" value="<?= $data["id_sub"]; ?>" hidden>
						</tr>
					</table>
					<button type="submit" name="ubahsubject" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
					<a href="subject.php" type="button" class="btn btn-danger"><i class="fa fa-undo"></i> Batal</a>
				</form>
			</div>

		<?php
		}
		?>


	</center>

</body>

</html>