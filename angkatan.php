<?php
if(isset($_GET["ANGKATAN"]) && isset($_GET["id_sub"])) {
  require_once 'angkatan_kelas.php';
} else {?>

<?php
require "template.php";

$date  = date('Y-m-d');
$diff  = strtotime($date); $tgl_f = date("d F Y", $diff);
$clock = date('H:i:s');

$subject = query("SELECT * FROM tabel_subject");


?>

<!-- datatables-->
<link rel="stylesheet" type="text/css" href="template/app-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="template/app-assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="template/app-assets/css/bootstrap-extended.css">


<link href="fontawesome/css/all.css" rel="stylesheet">

<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row"></div>
    <div class="content-body">
      <section id="headers">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">

                <?php
                   if(isset($_GET["ANGKATAN"])){
                    $ANGKATAN = mysqli_escape_string($koneksi, $_GET["ANGKATAN"]); 
                    // $ANGKATAN = $_GET["ANGKATAN"]; 
										$data   = query("SELECT * FROM tabel_subject WHERE ANGKATAN = '$ANGKATAN'");

                   ?>
                <h4 class="card-title">DATA PRESENSI ANGKATAN <?=convertRomawi($ANGKATAN).' ('.$ANGKATAN.')';?></h4>
                <?php } ?>

              </div>
              <div class="card-content">
                <div class="card-body card-dashboard">

                  <div class="table-responsive">
                    <table id="tabel-data" class="table  table-striped complex-headers">
                      <thead>
                        <tr class="bg-primary text-white text-center">
                        <th scope="col">No</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Angkatan</th>
                        <th scope="col">Jumlah Anggota</th>
                        <th scope="col">Opsi</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php 
                        $no = 1;
                        foreach ($data as $i) : 
                           //Menghitung jumlah subject
                           $id_sub   = $i["id_sub"];
                           $query    = "SELECT * FROM tabel_anggota WHERE id_sub = '$id_sub'";
                           $result   = mysqli_query($koneksi, $query);
                           $val      = mysqli_num_rows($result);

                        ?>
                      <tr class="text-center">
                        <th scope="row"><?=$no;?></th>
                        <td><?=$i["SUBJECT"];?></td>
                        <td><?=convertRomawi($i["ANGKATAN"]);?></td>
                        <td><?=$val;?></td>
                        <td>
                          <a class="btn btn-sm btn-success font-weight-bold text-uppercase" href="angkatan.php?ANGKATAN=<?=$ANGKATAN;?>&id_sub=<?=$i["id_sub"];?>">Lihat Presensi</a>
                        </td>
                      </tr>
                      <?php 
                           $no++;
                           endforeach; 
                      ?>

                      </tbody>
                    </table>
                  </div>



                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>



<script>
  $(document).ready(function () {
    $('#tabel-data').DataTable();
  });
</script>

<!-- BEGIN: Page JS-->
<script src="template/app-assets/js/scripts/datatables/datatable.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
<!-- END: Page JS-->
<!-- script -->

<?php
}
?>
