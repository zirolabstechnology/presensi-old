<?php 
require "koneksidb.php";

session_start();

if (!isset($_SESSION["login"])) {
    $TOKEN   = $pengaturan["TOKEN"];
    $ID_CHAT = $pengaturan["ID_CHAT"];
    $pesan   = "PERINGATAN!!!\n\nAda yang berusaha mengakses akun anda secara paksa (tanpa melalui login)";
    header("location:index.php");
    kirimPesan($ID_CHAT, $pesan, $TOKEN);
    exit;
}


header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Presensi Bulanan.xls");

$thn  = $_GET["tahun"];
$bln  = $_GET["bulan"];
$YM   = $thn."-".$bln;
$diff = strtotime($YM);
$TB   = date("F Y", $diff);  

$lengthday = cal_days_in_month(CAL_GREGORIAN, $bln, $thn); 

$dataanggota = query("SELECT * FROM tabel_anggota ORDER BY NAMA ASC"); 


?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
  <center>
  	<h3>PRESENSI BULANAN</h3>

<div class="table-responsive-sm">

<div class="row" style="width:90rem;">
  <div class="col">
       <p style="font-weight: bold">Periode: <?=$TB; ?></p>
  </div>
</div>

<table class="table table-bordered table-hover table-striped">
   <tr class="text-center text-white bg-dark"> 
   <th rowspan="2" class="py-3">No.</th>
   <th rowspan="2" class="py-3 px-5">Nama</th> 
   <th colspan="<?=$lengthday;?>" class="py-1">Tanggal</th>   
   </tr>
    <tr class="text-center text-white bg-dark">
       <?php  for($d=1; $d <= $lengthday; $d++){
          echo "<th class='py-1'>$d</th>";
      }?>
   </tr>
<?php $i =1;?>

<tr> 
   <?php 

      foreach ($dataanggota as $anggota) :
        $ID   = $anggota["ID"];
        $nama = $anggota["NAMA"];
        echo "<td>".$i."</td>
              <td>".$nama."</td>"; 

           for ($d=1; $d<=$lengthday; $d++) { 
            if ($d < 10){
                $d = "0".(String)$d;
              }
             $tgl  = date("Y-m-".$d, $diff);
             $read = query("SELECT * FROM tabel_kehadiran WHERE ID = '$ID' AND TANGGAL = '$tgl'");

              if($read){
                foreach ($read as $key) {
                    switch ($key['KET']) {
                      case 'HADIR': $col = "green";     break;
                       case 'SAKIT': $col = "yellow";    break;
                       case 'IZIN' : $col = "blue";      break;
                       case 'ALFA' : $col = "purple";   break;
                       case 'BOLOS': $col = "brown";    break;
                       case 'LUPA' : $col = "lightblue"; break;
                       case 'LIBUR': $col = "red";       break;
                       case ''     : $col = "";          break;
                    }
                       echo '<td style="background-color:'.$col.';"></td>';
                 }
              }
              else{
                echo '<td>-</td>';
              }

           }
    ?>
</tr>
   <?php $i++; ?>
  <?php endforeach;?>
</table>
</div>

  	


  </center>

  
	

</body>
</html>