<?php 

require "../koneksidb.php";

session_start();

if (!isset($_SESSION["login"])) {
    $TOKEN   = $pengaturan["TOKEN"];
    $ID_CHAT = $pengaturan["ID_CHAT"];
    $pesan   = "PERINGATAN!!!\n\nAda yang berusaha mengakses akun anda secara paksa (tanpa melalui login)";
    header("location:index.php");
    kirimPesan($ID_CHAT, $pesan, $TOKEN);
    exit;
}

$keywordanggota = $_GET["keywordanggota"];

$data    =  query("SELECT * FROM tabel_anggota WHERE ID LIKE '%$keywordanggota%' OR NO_INDUK 
          LIKE '%$keywordanggota%' OR NAMA LIKE '%$keywordanggota%' ORDER BY NAMA ASC");

 ?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title></title>
 </head>
 <body>

  <div class="table-responsive-sm">
<table class="table table-bordered table-hover table-dark table-striped">
   <tr class="text-center" style="background: purple;"> 
   <th>No.</th>
   <th>ID Card</th>
   <th>ID Chat</th>
   <th>No. Induk</th>
   <th>Nama Anggota</th>
   <th width="10px">L/P</th>
   <th>Subject</th>
   <th>Terdaftar</th>
   <th width="140px">Opsi</th>
   </tr>
<?php $i =1;?>

<?php foreach ($data as $anggota) : 
    $diff_tgl  = strtotime($anggota["TERDAFTAR"]);
    $terdaftar = date("d F Y", $diff_tgl);
    $id_sub = $anggota["id_sub"];
    $datasub = query("SELECT * FROM tabel_subject WHERE id_sub ='$id_sub' ");
?>
   <tr>
   <td class="text-center"><?= $i; ?></td>
   <td class="text-center"><?= $anggota["ID"];?></td>
   <td class="text-center"><?= $anggota["ID_CHAT"];?></td>
   <td class="text-center"><?= $anggota["NO_INDUK"];?></td>
   <td><?= $anggota["NAMA"];?></td>
   <td class="text-center"><?= $anggota["KELAMIN"];?></td>   
   <?php
       foreach ($datasub as $subject){ 
          echo '<td class="text-center">'.$subject["SUBJECT"].'</td>';
        }  
    ?>
   <td class="text-center"><?=$terdaftar;?></td>
   <td align="center">
      <a class="kehadiran btn btn-primary btn-sm" href="kehadiranperorang.php?ID=<?=$anggota["ID"]; ?>&NAMA=<?=$anggota["NAMA"];?>&ID_CHAT=<?=$anggota["ID_CHAT"];?>"><i class="fa fa-clipboard"></i></a>
      <a class="ubah btn btn-success btn-sm" href="ubahanggota.php?ID=<?=$anggota["ID"];?>"><i class="fa fa-edit"></i></a>
       <a class="hapus btn btn-danger btn-sm alert_hapus" href="hapus.php?ID=<?=$anggota["ID"];?>&ID_CHAT=<?=$anggota["ID_CHAT"];?>"><i class="fa fa-trash-alt"></i></a>
   </td>
   </tr>
   <?php $i++; ?>
  <?php endforeach; ?>

</table>
</div> 	
 
 </body>
 </html>