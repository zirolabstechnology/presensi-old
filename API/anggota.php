<?php
include_once('include.php');
// API menampilkan data anggota
if($koneksi==null){
    sendResponse(404, $koneksi,'server sudah move on');
}
else{
    $sql="SELECT * FROM tabel_anggota";
    $result=$koneksi->query($sql);
    if($result->num_rows > 0){
        $users=array();
        while($data=$result->fetch_assoc()){
            $user=array(
                "ID"       => $data ['ID'],
                "ID_CHAT"  => $data ['ID_CHAT'],
                "NO_INDUK" => $data ['NO_INDUK'],
                "NAMA"     => $data ['NAMA'],
                "KELAMIN"  => $data ['KELAMIN'],
                "id_sub"   => $data ['id_sub'],
                "TERDAFTAR"=> $data ['TERDAFTAR'],
                "gambar_anggota"=>$data['gambar_anggota'],
            );
            array_push($users, $user);
        }
        sendResponse(300, $users, 'Data Anggota');
    }
    else{
        sendResponse(500, [], 'belum ada data');
    }
    $koneksi->close();
}
?>