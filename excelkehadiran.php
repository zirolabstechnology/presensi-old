<?php

session_start();

if (!isset($_SESSION["login"])) {
    $TOKEN   = $pengaturan["TOKEN"];
    $ID_CHAT = $pengaturan["ID_CHAT"];
    $pesan   = "PERINGATAN!!!\n\nAda yang berusaha mengakses akun anda secara paksa (tanpa melalui login)";
    header("location:index.php");
    kirimPesan($ID_CHAT, $pesan, $TOKEN);
    exit;
}

require "koneksidb.php";

header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Data Presensi.xls");

 // $date          = date("Y-m-d"); //Tanggal hari ini
 // $datakehadiran = query("SELECT * FROM tabel_kehadiran WHERE TANGGAL ='$date' ORDER BY NAMA ASC"); 

$TANGGAL1      = $_GET["TANGGAL1"];
$TANGGAL2      = $_GET["TANGGAL2"];

$datakehadiran = query("SELECT * FROM tabel_kehadiran WHERE TANGGAL BETWEEN '$TANGGAL1' AND '$TANGGAL2' ORDER BY TANGGAL DESC");

?>

<!DOCTYPE html>
 <html>
 <head>
  <title></title>
 </head>
 <body>
    <center>
       <h3>DATA PRESENSI ANGGOTA</h3>
<div class="table-responsive-sm">
<table>
   <tr> 
       <th  rowspan="2">No.</th>
       <th  rowspan="2">No. Induk</th>
       <th  rowspan="2">Nama Anggota</th>
       <th  rowspan="2">Tanggal</th>
       <th  colspan="3">Jam Masuk</th>
       <th  colspan="3">Jam Pulang</th>
       <th  rowspan="2">Keterangan</th>
   </tr>
   <tr>
       <th >Masuk</th>
       <th >Check In</th>
       <th >Late In</th>
       <th >Pulang</th>
       <th >Check Out</th>
       <th >Early Out</th>
   </tr>
<?php 
    $i =1;
    foreach ($datakehadiran as $kehadiran) :
        $ID_kehadiran = $kehadiran["ID"];
        $diff_tgl     = strtotime($kehadiran["TANGGAL"]);
        $tanggal      = date("d F Y", $diff_tgl);
        $f_late_in    = date("H:i:s", $kehadiran["LATE_IN"] - $det);
        $f_early_out  = date("H:i:s", $kehadiran["EARLY_OUT"] - $det);
        $anggota      = query("SELECT * FROM tabel_anggota WHERE ID = '$ID_kehadiran'");

    if ($kehadiran["STAT"] =="alfa"){
       $sql = "UPDATE tabel_kehadiran SET JAM_MASUK = '$JAM_MASUK', CHECK_IN = 'empty', LATE_IN = 0, JAM_PULANG = '$JAM_PULANG', CHECK_OUT = 'empty', EARLY_OUT = 0, KET = 'ALFA' WHERE STAT = 'alfa'";
       $koneksi->query($sql); 
     }
    else if ($kehadiran["STAT"] =="bolos"){
       $sql = "UPDATE tabel_kehadiran SET JAM_PULANG = '$JAM_PULANG', CHECK_OUT = 'empty', EARLY_OUT = 0, KET = 'BOLOS' WHERE STAT = 'bolos'";
       $koneksi->query($sql); 
     }
   
     echo '<tr>
           <td class="text-center">'.$i.'</td>';
     
         foreach ($anggota as $ID_anggota){ 
            echo '<td class="text-center">'.$ID_anggota["NO_INDUK"].'</td>
                  <td class="text-center">'.$ID_anggota["NAMA"].'</td>';
          }  
 ?>
   <td><?= $tanggal;?></td>
   <td><?= $kehadiran["JAM_MASUK"];?></td>
   <td><?= $kehadiran["CHECK_IN"];?></td>
   <td><?= $f_late_in;?></td>
   <td><?= $kehadiran["JAM_PULANG"];?></td>
   <td><?= $kehadiran["CHECK_OUT"];?></td>
   <td><?= $f_early_out;?></td>
   <td><?= $kehadiran["KET"];?></td>
   </tr>
   <?php $i++; ?>
<?php endforeach; ?>

</table>
</div>
    </center>
 </body>
 </html>