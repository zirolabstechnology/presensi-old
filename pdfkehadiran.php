<?php 

require "koneksidb.php";

session_start();

if (!isset($_SESSION["login"])) {
    $TOKEN   = $pengaturan["TOKEN"];
    $ID_CHAT = $pengaturan["ID_CHAT"];
    $pesan   = "PERINGATAN!!!\n\nAda yang berusaha mengakses akun anda secara paksa (tanpa melalui login)";
    header("location:index.php");
    kirimPesan($ID_CHAT, $pesan, $TOKEN);
    exit;
}



$TANGGAL1      = $_GET["TANGGAL1"];
$TANGGAL2      = $_GET["TANGGAL2"];

$datakehadiran = query("SELECT * FROM tabel_kehadiran WHERE TANGGAL BETWEEN '$TANGGAL1' AND '$TANGGAL2' ORDER BY TANGGAL DESC");

// Require composer autoload
require_once __DIR__ . '/vendor/autoload.php';

// Define a default Landscape page size/format by name
$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 
                        'format' => 'A4-L',
                        'margin_top' => 0
                      ]);

$cetak = '<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<br>
  <center>
   <p><h2>DATA PRESENSI ANGGOTA</h2></p>
  <table border = "1" cellpadding = "8" cellspacing = "1">
   <tr class="bg-dark text-white text-center"> 
   <th class="py-3" rowspan="2">No.</th>
   <th class="py-3" rowspan="2">No. Induk</th>
   <th class="py-3" rowspan="2">Nama Anggota</th>
   <th class="py-3" rowspan="2">Tanggal</th>
   <th class="py-1" colspan="3">Jam Masuk</th>
   <th class="py-1" colspan="3">Jam Pulang</th>
   <th class="py-3" rowspan="2">Keterangan</th>
   </tr>
   <tr class="bg-dark text-white text-center"> 
   <th class="py-1">Masuk</th>
   <th class="py-1">Check In</th>
   <th class="py-1">Late In</th>
   <th class="py-1">Pulang</th>
   <th class="py-1">Check Out</th>
   <th class="py-1">Early Out</th>
   </tr>';
    
    $i = 1;
    foreach ($datakehadiran as $kehadiran) {
      $ID_kehadiran = $kehadiran["ID"];
      $diff_tgl = strtotime($kehadiran["TANGGAL"]);
      $tanggal  = date("d F Y", $diff_tgl);
      $f_late_in   = date("H:i:s", $kehadiran["LATE_IN"] - $det);
      $f_early_out = date("H:i:s", $kehadiran["EARLY_OUT"] - $det);
      $anggota      = query("SELECT * FROM tabel_anggota WHERE ID = '$ID_kehadiran'");

    	$cetak .= '<tr>
    			   <td>'.$i.'</td>';

      foreach ($anggota as $ID_anggota){ 
          $cetak .= '<td class="text-center">'.$ID_anggota["NO_INDUK"].'</td>
                     <td class="text-center">'.$ID_anggota["NAMA"].'</td>';
       } 

      $cetak.='<td>'.$tanggal.'</td>
               <td>'.$kehadiran["JAM_MASUK"].'</td>
               <td>'.$kehadiran["CHECK_IN"].'</td>
               <td>'.$f_late_in.'</td>
               <td>'.$kehadiran["JAM_PULANG"].'</td>
               <td>'.$kehadiran["CHECK_OUT"].'</td>
               <td>'.$f_early_out.'</td>
               <td>'.$kehadiran["KET"].'</td>
    	     </tr>';
       $i++;
       }
$cetak .= '</table>
            </center>
               </body>
         </html>';


// Write some HTML code:
$mpdf->WriteHTML($cetak);
// Output a PDF file directly to the browser
$mpdf->Output('data presensi.pdf', \Mpdf\Output\Destination::DOWNLOAD);

 ?>