<?php
require "template.php";
// include "kehadiran-logic.php";

$TANGGAL1 = date("Y-m-d");
$TANGGAL2 = date("Y-m-d");

$bln = date("m");
$thn = date("Y");

?>



<!DOCTYPE html>
<html>

<head>
  <title></title>
</head>

<body>
  <center>
    <h3>DATA PRESENSI ANGGOTA</h3>
    <br>

    <div class="row">
      <div class="col">
        <!-- Filter data -->
        <form method="get" action="kehadiran-filter.php">
          <input type="date" name="TANGGAL1"> s/d
          <input type="date" name="TANGGAL2">
          <input type="submit" value="Filter">
        </form>
      </div>

      <div class="col">
        <!-- Export data -->
        <div class="dropdown">
          <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" style="background:#008080; color:white"><i class="fa fa-download"></i> Export Data
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="pdfkehadiran.php?TANGGAL1=<?= $TANGGAL1; ?>&TANGGAL2=<?= $TANGGAL2; ?>"><i class="fa fa-file-pdf"></i> Export to PDF</a>
            <a class="dropdown-item" href="excelkehadiran.php?TANGGAL1=<?= $TANGGAL1; ?>&TANGGAL2=<?= $TANGGAL2; ?>"><i class="fa fa-file-excel"></i> Export to Excel</a>
          </div>
          <a type="button" class="btn btn-danger mx-2" href="rekapdata.php?bulan=<?= $bln; ?>&tahun=<?= $thn; ?>"><i class="fa fa-database"></i> Rekap Data</a>
        </div>
      </div>

    </div>

    <br>

    <div class="kehadiran-value"></div>


  </center>

</body>

</html>