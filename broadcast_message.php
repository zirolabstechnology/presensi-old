<?php

use Mpdf\Tag\Select;

// include 'koneksidb.php';
require 'template.php';
include 'send_wa.php';

$date  = date('Y-m-d');
$diff  = strtotime($date);
$tgl_f = date("d F Y", $diff);
$clock = date('H:i:s');

// $query_alfa      = "SELECT * FROM tabel_kehadiran WHERE TANGGAL = '$date' AND KET = 'ALFA'";
// $result_alfa     = mysqli_query($koneksi, $query_alfa);
// $alfa       = mysqli_num_rows($result_alfa);

// $query_hadir      = "SELECT * FROM tabel_kehadiran WHERE TANGGAL = '$date' AND KET = 'HADIR'";
// $result_hadir     = mysqli_query($koneksi, $query_hadir);
// $hadir       = mysqli_num_rows($result_hadir);

// $query_sakit      = "SELECT * FROM tabel_kehadiran WHERE TANGGAL = '$date' AND KET = 'SAKIT'";
// $result_sakit     = mysqli_query($koneksi, $query_sakit);
// $sakit       = mysqli_num_rows($result_sakit);

// $query_izin      = "SELECT * FROM tabel_kehadiran WHERE TANGGAL = '$date' AND KET = 'IZIN'";
// $result_izin     = mysqli_query($koneksi, $query_izin);
// $izin       = mysqli_num_rows($result_izin);

// $pesan =  "
//     Pemberitahuan kepada bapak/ibu guru tim monitoring, Jumlah siswa dengan keterangan hadir sebanyak $hadir siswa, Siswa sakit sebanyak $sakit siswa, siswa izin $izin siswa, dan siswa tidak masuk tanpa keterangan sebanyak $alfa siswa. Terima Kasih
// ";

$data = query("SELECT WA_PRIBADI FROM tabel_anggota WHERE Level = 'Guru' ORDER BY NAMA ASC");
$array = array_map('array_pop', $data);
// var_dump($array);

$number =  implode(',', $array);

$rekap = [];
$data_kelas    = query("SELECT * FROM tabel_subject WHERE ANGKATAN IS NOT NULL ORDER BY ANGKATAN ASC");
foreach ($data_kelas as $kelas) {
    $id_kelas = $kelas['id_sub'];
    $absen_alfa = mysqli_query($koneksi, "SELECT * from tabel_kehadiran Where TANGGAL = '$date' And id_sub = $id_kelas And KET = 'ALFA'");
    $absen_hadir = mysqli_query($koneksi, "SELECT * from tabel_kehadiran Where TANGGAL = '$date' And id_sub = $id_kelas And KET = 'HADIR'");
    $absen_sakit = mysqli_query($koneksi, "SELECT * from tabel_kehadiran Where TANGGAL = '$date' And id_sub = $id_kelas And KET = 'SAKIT'");
    $absen_izin = mysqli_query($koneksi, "SELECT * from tabel_kehadiran Where TANGGAL = '$date' And id_sub = $id_kelas And KET = 'IZIN'");
    // var_dump($absen);
    $total_alfa = mysqli_num_rows($absen_alfa);
    $total_hadir = mysqli_num_rows($absen_hadir);
    $total_izin = mysqli_num_rows($absen_izin);
    $total_sakit = mysqli_num_rows($absen_sakit);
    array_push($rekap, $kelas['SUBJECT'] . " Memiliki " . $total_hadir . " siswa hadir, " . $total_alfa . " siswa Alfa, " . $total_izin . " siswa izin, " . $total_sakit . " siswa sakit \n");
}

// print("<pre>" . print_r($rekap, true) . "</pre>");
$pesan = implode('', $rekap);
send($number, "Pemberitahuan kepada bapak/ibu guru tim monitoring hasil rekap hari ini sebagai berikut $pesan \n Terimakasih!");
echo "
<script> 
Swal.fire({ 
   title: 'Selamat', 
   text: 'Pesan Berhasil dirikirim! ', 
   icon: 'success', 
   dangerMode: true, 
   buttons: [false, 'OK'], 
   }).then(function() { 
       window.location.href='dataguru.php'; 
   }); 
</script>
";
?>
<!DOCTYPE html>
<html lang="en">

<head></head>

<body>
    <script type="text/javascript">
        // location.href = 'dataguru.php';
    </script>
</body>

</html>