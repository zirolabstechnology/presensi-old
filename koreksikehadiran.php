<?php

require "template.php";

$TOKEN = $pengaturan["TOKEN"];


	 if(isset($_POST["simpan"]) ) {
	   if(koreksikehadiran($_POST) > 0 ) {
       $pesan="Hai ".$_POST["NAMA"]."\n\nStatus Kehadiran kamu pada Tanggal: ".$_POST["TANGGAL"]." telah diperbaharui dari ".$_POST["KET_1"]." menjadi ".$_POST["KET_2"]."\n\nData diperbarui pada: \n".date("d F Y H:i:s")."\n\nSilakan konfirmasi ke Admin";
		echo "
			 <script>
				  Swal.fire({ 
                  title: 'SELAMAT',
                  text: 'Perubahan data telah disimpan',
                  icon: 'success', buttons: [false, 'OK'], 
                  }).then(function() { 
                  window.location.href='dataanggota.php'; 
                  }); 
			 </script>
		";
	   }
	   else {
	    echo "
         <script> 
         Swal.fire({ 
            title: 'OOPS', 
            text: 'Data gagal ditambahkan', 
            icon: 'warning', 
            dangerMode: true, 
            buttons: [false, 'OK'], 
            }).then(function() { 
                window.location.href='dataanggota.php'; 
            }); 
         </script>
        ";
	   }
     if($pengaturan["SW"] == 1){
        kirimpesan($_POST["ID_CHAT"], $pesan, $TOKEN);
     }
	 }




?>


<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<center>
		<h3 class="text-center">KOREKSI KEHADIRAN</h3>

    <br>

    <?php 
      if(isset($_GET["no"])){
         $no      = $_GET["no"];
         $ID_CHAT = $_GET["ID_CHAT"];
         $data    = query("SELECT * FROM tabel_kehadiran WHERE no = '$no'")[0];
    ?>

    <div class="card" style="width: 25rem;">
      <div class="card-body bg-dark text-white">
          <form action="koreksikehadiran.php" method="post">
             <input class="form-control bg-dark text-white" name="no" type="text" autocomplete="off" value="<?=$data["no"]?>" hidden>
            <div class="table-responsive-sm">
             <table class="table bg-dark text-white">
              <tr>
                <td>No Induk</td>
                <td>:</td>
                <td><?=$data["NO_INDUK"];?></td>
              </tr>
              <tr>
                <td>Nama</td>
                <td>:</td>
                <td><?=$data["NAMA"];?></td>
                <input type="text" name="NAMA" value="<?=$data["NAMA"];?>" hidden>
              </tr>
              <tr>
                <td>Tanggal</td>
                <td>:</td>
                <td><?=$data["TANGGAL"];?></td>
                <input type="text" name="TANGGAL" value="<?=$data["TANGGAL"];?>" hidden>
              </tr>
              <tr>
                <td>Check In</td>
                <td>:</td>
                <td><?=$data["CHECK_IN"]?></td>
              </tr>
              <tr>
                <td>Check Out</td>
                <td>:</td>
                <td><?=$data["CHECK_OUT"]?></td>
              </tr>
              <tr>
                <input type="text" name="KET_1" value="<?=$data["KET"];?>" hidden>
                <td>Keterangan</td>
                <td>:</td>
                <td><div class="input-group">

                  <?php 
                            $val   = $data["KET"];
                            $hadir = "";
                            $sakit = "";
                            $izin  = "";
                            $alfa  = "";
                            $bolos = "";
                            $lupa  = "";
                            $libur = "";

                      switch ($val) {
                        case 'HADIR':
                            $hadir = "selected";
                          break;

                          case 'SAKIT':
                            $sakit = "selected";
                          break;

                          case 'IZIN':
                            $izin = "selected";
                          break;

                          case 'ALFA':
                            $alfa = "selected";
                          break;

                          case 'BOLOS':
                            $bolos = "selected";
                          break;

                          case 'LUPA':
                            $lupa = "selected";
                          break;

                          case 'LIBUR':
                            $libur = "selected";
                          break;
                      }
                   ?>
                        <div class="input-group-prepend"></div>
                           <select name="KET_2" class="custom-select bg-dark text-white">
                            <option>---Pilih Keterangan---</option>
                            <option <?=$hadir;?> value="HADIR">Hadir</option>
                            <option <?=$sakit;?> value="SAKIT">Sakit</option>
                            <option <?=$izin;?> value="IZIN">Izin</option>
                            <option <?=$alfa;?> value="ALFA">Alfa</option>
                            <option <?=$bolos;?> value="BOLOS">Bolos</option>
                            <option <?=$lupa;?> value="LUPA">Lupa</option>
                            <option <?=$libur;?> value="LIBUR">Libur</option>
                          </select>
                    </div>
                    <input type="text" name="ID_CHAT" value="<?=$ID_CHAT;?>" hidden>
                </td>
              </tr>
             </table>
            </div>  
              <button type="submit" name="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
              <a href="dataanggota.php"  name="batal" class="btn btn-danger"><i class="fa fa-undo"></i> Batal</a>       
          </form>
      </div>
    </div>
<?php } ?>
 
             
 </center>
    
   

</body>
</html>