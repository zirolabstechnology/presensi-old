<?php 
    include("koneksidb.php");
    //Waktu
	$date  = date('Y-m-d');
	 ?>


<!-- <div class="card"> -->
    <!-- <div class="card-header">
      <h3 class="card-title">DataTable with default features</h3>
    </div> -->
    <!-- /.card-header -->
    <!-- <div class="card-body"> -->
      <table id="employee_data" class="table table-striped table-bordered">
        <thead>
        <tr>
			<th style="text-align: center;">Foto</th>
			<th style="text-align: center;">Name</th>
			<th style="text-align: center;">Tanggal</th>
			<th style="text-align: center;">Check In</th>
			<th style="text-align: center;">Check Out</th>
			<th style="text-align: center;">Keterangan</th>
        </tr>
        </thead>
        <?php 
	// $query = mysqli_query($koneksi, "SELECT * FROM tabel_kehadiran INNER JOIN tabel_anggota on tabel_kehadiran.ID = tabel_anggota.ID");
	$query = mysqli_query($koneksi, "SELECT *
									 FROM tabel_kehadiran, tabel_anggota WHERE tabel_kehadiran.ID = tabel_anggota.ID
									 AND tabel_kehadiran.TANGGAL = '$date'");

	while($row = mysqli_fetch_array($query)){

        $diff_tgl = strtotime($row["TANGGAL"]);

		$name = $row['NAMA'];
        $tanggal  = date("d F Y", $diff_tgl);
		$jam_masuk = $row['CHECK_IN'];
		$jam_pulang = $row['CHECK_OUT'];
		$keterangan = $row['KET'];
        $img = $row['gambar_anggota'];
 ?>
			<tbody>
				<tr>
					<td style="text-align: center;"><img src="img/<?php echo $img; ?>" style="width: 100px;"  alt="no image"></td></td>
					<td style="text-align: center;"><?php echo $name; ?></td>
					<td style="text-align: center;"><?php echo $tanggal; ?></td>
					<td style="text-align: center;"><?php echo $jam_masuk; ?></td>
					<td style="text-align: center;"><?php echo $jam_pulang; ?></td>
					<td style="text-align: center;"><?php echo $keterangan; ?></td>
				</tr>
			</tbody>
<?php 	} ?>
      </table>
    <!-- </div> -->
    <!-- /.card-body -->
<!-- </div> -->

<script>
  $(function () {
    $("#employee_data").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
  });
</script>
