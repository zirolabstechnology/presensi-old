<?php
require "template.php";

$date  = date('Y-m-d');
$diff  = strtotime($date); $tgl_f = date("d F Y", $diff);
$clock = date('H:i:s');
$Tanggal1 = date("Y-m-d");
$Tanggal2 = date("Y-m-d");


$subject = query("SELECT * FROM tabel_subject");


?>

<!-- datatables-->
<link rel="stylesheet" type="text/css" href="template/app-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="template/app-assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="template/app-assets/css/bootstrap-extended.css">


<link href="fontawesome/css/all.css" rel="stylesheet">

<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row"></div>
    <div class="content-body">
      <section id="headers">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">

                <?php
                   if(isset($_GET["ANGKATAN"]) && isset($_GET['id_sub'])){
                    $ANGKATAN = mysqli_escape_string($koneksi, $_GET["ANGKATAN"]); 
										$data   = query("SELECT * FROM tabel_subject WHERE ANGKATAN = '$ANGKATAN'")[0];
										//Menghitung jumlah subject
										$sub      = $data["SUBJECT"];
										$angkatan      = $data["ANGKATAN"];
                    $query    = "SELECT * FROM tabel_anggota WHERE ANGKATAN = '$ANGKATAN'";
                    
                    $id_sub = mysqli_escape_string($koneksi, $_GET["id_sub"]);
                    $sql_kls = mysqli_query($koneksi, "SELECT SUBJECT FROM tabel_subject WHERE id_sub = '$id_sub'");
                    while($dataKls = mysqli_fetch_array($sql_kls)) {
                      $kelas = $dataKls['SUBJECT'];
                    }
                ?>
                <h4 class="card-title">DATA PRESENSI ANGKATAN <?= convertRomawi($ANGKATAN).' ('.$ANGKATAN.')'; ?></h4>
                <h5 class="card-title text-uppercase">Kelas <?= $kelas; ?></h5>
                <?php } ?>
              
              </div>
              
              <div class="card-content">
                <div class="card-body card-dashboard">
                    
            <!--<button type="button" class="btn btn-icon btn-info mr-1 mb-1" href="#cetak" data-toggle="modal" data-placement="bottom" title="cetak pdf" data-target="#cetak"><i class="feather icon-printer"></i></button>-->

                  <div class="table-responsive">
                    <table class="table  table-striped dataex-html5-selectors">
                      <thead>
                        <tr class="bg-primary text-white text-center">
                          <th class="text-center">ID</th>
                          <th class="text-center">Name</th>
                          <th class="text-center">Tanggal</th>
                          <th class="text-center">Check In</th>
                          <th class="text-center">Check Out</th>
                          <th class="text-center">Keterangan</th>
                          <th class="text-center">Suhu</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i =1;?>

                        <?php 
            // $query = mysqli_query($koneksi, "SELECT * FROM tabel_kehadiran INNER JOIN tabel_anggota on tabel_kehadiran.ID = tabel_anggota.ID");
            $kelas = $_GET['id_sub'];
            // $query = mysqli_query($koneksi, "SELECT *
            //                 FROM tabel_kehadiran, tabel_anggota WHERE tabel_kehadiran.ID = tabel_anggota.ID
            //                 AND tabel_kehadiran.TANGGAL = '$date' AND id_sub='$kelas'");
                          
            $query = mysqli_query($koneksi, "SELECT *
                      FROM tabel_kehadiran, tabel_anggota WHERE tabel_anggota.id_sub = '$id_sub' ORDER BY TANGGAL DESC, tabel_kehadiran.NAMA");

            while($row = mysqli_fetch_array($query)){

                  $diff_tgl = $row["TANGGAL"];

                  $name = $row['NAMA'];
                  $tanggal  = date("d F Y", $diff_tgl);
                  $jam_masuk = $row['CHECK_IN'];
                  $jam_pulang = $row['CHECK_OUT'];
                  $keterangan = $row['KET'];
                  $kls = $row['id_sub'];
                  $suhu =$row['suhu'];
          ?>
                        <tr>
                          <td class="text-center"><?= $i; ?></td>
                          <td class="text-center"><?= $name; ?></td>
                          <td class="text-center"><?= $diff_tgl; ?></td>
                          <td class="text-center"><?= $jam_masuk; ?></td>
                          <td class="text-center"><?= $jam_pulang; ?></td>
                          <td class="text-center"><?= $keterangan; ?></td>
                          <!--  -->

                          <td class="text-center"><?= $suhu; ?> &deg;C</td>

                        </tr>
                        <?php $i++; ?>
                        <?php 	} ?>
                      </tbody>
                    </table>
                  </div>



                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>


<div class="modal fade text-left" id="cetak" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-primary" id="myModalLabel33"><i class="feather icon-printer"></i>  Cetak Pdf</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="cetak_siswa.php" method="post" target="_blank">
                <div class="modal-body">
                    <table>
                        <tr>
                            <td>
                                <div class="form-group">Dari Tanggal</div>
                            </td>
                            <td align="center">
                                <div class="form-group">:</div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="date" class="form-control pickadate" name="TANGGAL1" required>
                                </div>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <div class="form-group">Sampai Tanggal</div>
                            </td>
                            <td align="center">
                                <div class="form-group">:</div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="date" class="form-control pickadate" name="TANGGAL2" required>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <input type="submit" name="cetak_siswa" class="btn btn-primary btn-sm" value="Cetak">
                            </td>
                        </tr>
                    </table>
            </form>
        </div>
    </div>
</div>



<script>
  $(document).ready(function () {
    $('#tabel-data').DataTable();
  });
</script>

<!-- BEGIN: Page JS-->
<script src="template/app-assets/js/scripts/datatables/datatable.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
<script src="template/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
<!-- END: Page JS-->
<!-- script -->