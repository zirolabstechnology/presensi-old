<?php

require "template.php";

$TOKEN = $pengaturan["TOKEN"];


if (isset($_POST["simpan"])) {
  if (ubahGuru($_POST) > 0) {
    // $id_sub  = $_POST["id_sub"];
    // $datasub = query("SELECT * FROM tabel_subject WHERE id_sub = '$id_sub'")[0];
    $pesan = "Data Diri Anda Telah diperbarui\n\nNama: " . $_POST["NAMA"] . "\nNo. Induk: " . $_POST["NO_INDUK"] . "\nGender: " . $_POST["KELAMIN"] . "\n\nData diperbarui pada: \n" . date("d F Y H:i:s") . "\n\nSegera laporkan ke admin jika terjadi kesalahan input data. Terimakasih";
    echo "
			 <script>
				  Swal.fire({ 
                  title: 'SELAMAT',
                  text: 'Perubahan data telah disimpan',
                  icon: 'success', buttons: [false, 'OK'], 
                  }).then(function() { 
                  window.location.href='dataguru.php'; 
                  }); 
			 </script>
		";
  } else {
    echo "
         <script> 
         Swal.fire({ 
            title: 'OOPS', 
            text: 'Data gagal diperbarui', 
            icon: 'warning', 
            dangerMode: true, 
            buttons: [false, 'OK'], 
            }).then(function() { 
                window.location.href='dataguru.php'; 
            }); 
         </script>
        ";
  }
  if ($pengaturan["SW"] == 1) {
    kirimPesan($_POST["ID_CHAT"], $pesan, $TOKEN);
  }
}


?>

<link href="fontawesome/css/all.css" rel="stylesheet">
<div class="card mx-auto bg-dark text-white" style="width: 25rem;">
  <div class="card-header bg-dark text-white">
    <h4 class="card-title">UBAH DATA GURU</h4>
  </div>
  <div class="card-content bg-dark text-white">
    <?php
    if (isset($_GET["ID"])) {
      $ID       = mysqli_escape_string($koneksi, $_GET["ID"]);
      $data     = query("SELECT * FROM tabel_anggota WHERE ID = '$ID'")[0];
    ?>
      <div class="card-body bg-dark text-white">
        <h3 class="card-title">ID Card: <span class="text-primary"><?= $data["ID"]; ?></span></h3>
        <form action="ubahguru.php" method="post">
          <div class="form-group">
            <input type="text" name="ID" class="form-control bg-dark text-white" value="<?= $data["ID"]; ?>" hidden><br>
            <input type="text" name="ID_CHAT" class="form-control bg-dark text-white" placeholder="Masukkan ID Chat...." autocomplete="off" value="<?= $data["ID_CHAT"]; ?>"><br>
            <input type="text" name="NO_INDUK" class="form-control bg-dark text-white" placeholder="Masukkan NIP...." autocomplete="off" value="<?= $data["NO_INDUK"]; ?>"><br>
            <input type="text" name="NAMA" class="form-control bg-dark text-white" placeholder="Masukkan Nama...." autocomplete="off" value="<?= $data["NAMA"]; ?>"><br>
            <input type="number" name="WA_PRIBADI" class="form-control bg-dark text-white" placeholder="Masukkan Whatsapp Anda...." autocomplete="off" value="<?= $data["WA_PRIBADI"]; ?>"><br>

            <div class="row">
              <?php if ($data["KELAMIN"] == "L") {
                echo '
                                  <div class ="col">
                                    <div class="form-check">
                                        <input class="form-check-input bg-dark text-white" type="radio" name="KELAMIN" value="L" checked="checked">
                                        <label class="form-check-label">Laki laki</label>
                                    </div>
                                  </div>
                                  <div class ="col">
                                    <div class="form-check">
                                        <input class="form-check-input bg-dark text-white" type="radio" name="KELAMIN" value="P">
                                        <label class="form-check-label">Perempuan</label>
                                    </div>
                                  </div>
                                ';
              } else if ($data["KELAMIN"] == "P") {
                echo '
                                  <div class ="col">
                                    <div class="form-check">
                                        <input class="form-check-input bg-dark text-white" type="radio" name="KELAMIN" value="L">
                                        <label class="form-check-label">Laki laki</label>
                                    </div>
                                  </div>
                                  <div class ="col">
                                    <div class="form-check">
                                        <input class="form-check-input bg-dark text-white" type="radio" name="KELAMIN" value="P" checked="checked">
                                        <label class="form-check-label">Perempuan</label>
                                    </div>
                                  </div>
                                ';
              }
              ?>
            </div>
            <br>
            <div class="text-center">
              <button type="submit" name="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
              <a href="dataguru.php" name="batal" class="btn btn-danger"><i class="fa fa-undo"></i> Batal</a>
            </div>
          </div>
        </form>
      </div>
    <?php   } ?>

  </div>

</div>


<script src="js/script.js"></script>