-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2020 at 11:52 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabel_anggota`
--

CREATE TABLE `tabel_anggota` (
  `ID` varchar(20) NOT NULL,
  `ID_CHAT` varchar(20) NOT NULL,
  `NO_INDUK` varchar(20) NOT NULL,
  `NAMA` varchar(70) NOT NULL,
  `KELAMIN` enum('L','P') NOT NULL,
  `id_sub` int(20) NOT NULL,
  `TERDAFTAR` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_hari_libur`
--

CREATE TABLE `tabel_hari_libur` (
  `H_LIBUR_1` varchar(20) NOT NULL,
  `H_LIBUR_2` varchar(20) NOT NULL,
  `T_LIBUR_3` varchar(20) NOT NULL,
  `T_LIBUR_4` varchar(20) NOT NULL,
  `T_LIBUR_5` varchar(20) NOT NULL,
  `T_LIBUR_6A` varchar(20) NOT NULL,
  `T_LIBUR_6B` varchar(20) NOT NULL,
  `T_LIBUR_7A` varchar(20) NOT NULL,
  `T_LIBUR_7B` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_hari_libur`
--

INSERT INTO `tabel_hari_libur` (`H_LIBUR_1`, `H_LIBUR_2`, `T_LIBUR_3`, `T_LIBUR_4`, `T_LIBUR_5`, `T_LIBUR_6A`, `T_LIBUR_6B`, `T_LIBUR_7A`, `T_LIBUR_7B`) VALUES
('Saturday', 'Sunday', '01 January 1970', '01 January 1970', '01 January 1970', '01 January 1970', '01 January 1970', '01 January 1970', '01 January 1970');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_kehadiran`
--

CREATE TABLE `tabel_kehadiran` (
  `no` int(200) NOT NULL,
  `ID` varchar(20) NOT NULL,
  `NO_INDUK` varchar(20) NOT NULL,
  `NAMA` varchar(20) NOT NULL,
  `TANGGAL` varchar(20) NOT NULL,
  `JAM_MASUK` varchar(20) NOT NULL,
  `CHECK_IN` varchar(20) NOT NULL,
  `LATE_IN` int(20) NOT NULL,
  `JAM_PULANG` varchar(20) NOT NULL,
  `CHECK_OUT` varchar(20) NOT NULL,
  `EARLY_OUT` int(20) NOT NULL,
  `KET` varchar(20) NOT NULL,
  `STAT` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_pengaturan`
--

CREATE TABLE `tabel_pengaturan` (
  `idbaru` varchar(20) NOT NULL,
  `ZONA` varchar(20) NOT NULL,
  `JAM_MASUK_1` varchar(20) NOT NULL,
  `JAM_MASUK_2` varchar(20) NOT NULL,
  `JAM_MASUK_3` varchar(20) NOT NULL,
  `JAM_PULANG_1` varchar(20) NOT NULL,
  `JAM_PULANG_2` varchar(20) NOT NULL,
  `JAM_PULANG_3` varchar(20) NOT NULL,
  `TOKEN` varchar(100) NOT NULL,
  `KEY_API` varchar(100) NOT NULL,
  `ID_CHAT` varchar(20) NOT NULL,
  `SW` int(20) NOT NULL,
  `SW_2` int(20) NOT NULL,
  `USERNAME` varchar(20) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_pengaturan`
--

INSERT INTO `tabel_pengaturan` (`idbaru`, `ZONA`, `JAM_MASUK_1`, `JAM_MASUK_2`, `JAM_MASUK_3`, `JAM_PULANG_1`, `JAM_PULANG_2`, `JAM_PULANG_3`, `TOKEN`, `KEY_API`, `ID_CHAT`, `SW`, `SW_2`, `USERNAME`, `PASSWORD`) VALUES
('', 'Asia/Makassar', '06:30:00', '09:30:00', '12:00:00', '13:30:00', '14:00:00', '15:00:00', '1292117779:AAHaF5vrp43ryZSvTziNpMbixNRHgbnuYyk', 'abc123', '441884684', 1, 1, 'admin', '$2y$10$x0Frc0pVCmNL4wOco0aJweezXEHe7Fu52dGyLlKb/moFPvlMpEpBi');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_subject`
--

CREATE TABLE `tabel_subject` (
  `id_sub` int(20) NOT NULL,
  `SUBJECT` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_subject`
--

INSERT INTO `tabel_subject` (`id_sub`, `SUBJECT`) VALUES
(1, 'Administrasi'),
(4, 'Driver'),
(5, 'Engineer'),
(9, 'Peneliti'),
(10, 'Chef'),
(11, 'Office Boy'),
(12, 'Keuangan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabel_anggota`
--
ALTER TABLE `tabel_anggota`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `NO_INDUK` (`NO_INDUK`),
  ADD KEY `NAMA` (`NAMA`),
  ADD KEY `id_sub` (`id_sub`);

--
-- Indexes for table `tabel_hari_libur`
--
ALTER TABLE `tabel_hari_libur`
  ADD PRIMARY KEY (`H_LIBUR_1`);

--
-- Indexes for table `tabel_kehadiran`
--
ALTER TABLE `tabel_kehadiran`
  ADD PRIMARY KEY (`no`),
  ADD KEY `ID` (`ID`),
  ADD KEY `NO_INDUK` (`NO_INDUK`),
  ADD KEY `NAMA` (`NAMA`);

--
-- Indexes for table `tabel_pengaturan`
--
ALTER TABLE `tabel_pengaturan`
  ADD PRIMARY KEY (`idbaru`);

--
-- Indexes for table `tabel_subject`
--
ALTER TABLE `tabel_subject`
  ADD PRIMARY KEY (`id_sub`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabel_kehadiran`
--
ALTER TABLE `tabel_kehadiran`
  MODIFY `no` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=515;

--
-- AUTO_INCREMENT for table `tabel_subject`
--
ALTER TABLE `tabel_subject`
  MODIFY `id_sub` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tabel_kehadiran`
--
ALTER TABLE `tabel_kehadiran`
  ADD CONSTRAINT `tabel_kehadiran_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `tabel_anggota` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tabel_kehadiran_ibfk_2` FOREIGN KEY (`NO_INDUK`) REFERENCES `tabel_anggota` (`NO_INDUK`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tabel_kehadiran_ibfk_3` FOREIGN KEY (`NAMA`) REFERENCES `tabel_anggota` (`NAMA`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
